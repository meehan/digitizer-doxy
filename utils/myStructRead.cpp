#include <iostream>
#include <stdio.h>
#include <string.h>



struct Header{
  int start;
  int stop;
}  __attribute__((__packed__));


struct EventFragment{
  Header header;
  uint32_t payload[];
}  __attribute__((__packed__));

#define NREADS 5



void TranslateFragment( uint32_t raw_payload[], EventFragment*& ev2){

  std::cout<<"\n\nTranslateFragment"<<std::endl;

  int total_size = sizeof(Header) + sizeof(uint32_t) * NREADS;

  ev2 = (EventFragment*)malloc(total_size);


  std::cout<<"printing ev2 before"<<std::endl;
  std::cout<<std::dec<<ev2->header.start<<std::endl;
  std::cout<<std::dec<<ev2->header.stop <<std::endl;
  std::cout<<std::hex<<ev2->payload[0] <<std::endl;


  std::cout<<"copy the event"<<std::endl;

  std::cout<<"printing ev1 copy"<<std::endl;
  std::cout<<std::hex<<raw_payload[0] <<std::endl;

  ev2->header.start = 23;
  ev2->header.stop  = 48;
  memcpy( ev2->payload, raw_payload, 4*NREADS);

  std::cout<<"printing ev2 after"<<std::endl;
  std::cout<<std::dec<<ev2->header.start<<std::endl;
  std::cout<<std::dec<<ev2->header.stop <<std::endl;
  std::cout<<std::hex<<ev2->payload[0] <<std::endl;


  std::cout<<std::dec<<std::endl;

}


int main(){
  std::cout<<"STARTING the program"<<std::endl;





  uint32_t raw_payload[NREADS];

  raw_payload[0] = 0x11119384;
  raw_payload[1] = 0x111113F2;
  raw_payload[2] = 0x11112384;
  raw_payload[3] = 0x11113330;
  raw_payload[4] = 0x11114684;




  int total_size = sizeof(Header) + sizeof(uint32_t) * NREADS;

  EventFragment* sam;
  sam = (EventFragment*)malloc(total_size);

  sam->header.start = 35;
  sam->header.stop  = 92;
  memcpy( sam->payload, raw_payload, 4*NREADS);

  std::cout<<"sam head : "<<std::dec<<sam->header.start<<"  "<<sam->header.stop<<std::endl;
  std::cout<<"sam pay  : "<<std::hex<<sam->payload[0]<<"  "<<sam->payload[1]<<std::endl;

  EventFragment* pat;


  std::cout<<std::dec<<"outside1"<<std::endl;
//  std::cout<<std::dec<<pat->header.start<<std::endl;
//  std::cout<<std::dec<<pat->header.stop<<std::endl;

  TranslateFragment( raw_payload, pat );

  std::cout<<"outside2"<<std::endl;
  std::cout<<std::dec<<pat->header.start<<std::endl;
  std::cout<<std::dec<<pat->header.stop<<std::endl;
  std::cout<<std::hex<<pat->payload[0]<<std::endl;

  std::cout<<"STOPPING the program"<<std::endl;




  return 0;
}