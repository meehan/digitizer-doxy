//============================================================================
// Name        : sis3153eth_access_test.cpp
// Author      : th
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

// #ifdef ETHERNET_VME_INTERFACE
//   #include "sis3153ETH_vme_class.h"
//   sis3153eth *gl_vme_crate;
//   char  gl_sis3153_ip_addr_string[32] = "128.141.48.76";
//
//   #ifdef LINUX
//     #include <sys/types.h>
//     #include <sys/socket.h>
//   #endif
//
//   #ifdef WINDOWS
//   #include <winsock2.h>
//   #pragma comment(lib, "ws2_32.lib")
//   typedef int socklen_t;
//   #endif
// #endif


//unsigned int gl_dma_buffer[0x10000] ;



int GetBufferLength( unsigned int BUFFER_CODE ){

  int NBuffer = -1;
  int kSize   = 1024*1024;
  int MSize   = 1048576; // 1024*1024 - one Mb in computer

  if(BUFFER_CODE==0)
    NBuffer=1;
  else if(BUFFER_CODE==1)
    NBuffer=2;
  else if(BUFFER_CODE==2)
    NBuffer=4;
  else if(BUFFER_CODE==3)
    NBuffer=8;
  else if(BUFFER_CODE==4)
    NBuffer=16;
  else if(BUFFER_CODE==5)
    NBuffer=32;
  else if(BUFFER_CODE==6)
    NBuffer=64;
  else if(BUFFER_CODE==7)
    NBuffer=128;
  else if(BUFFER_CODE==8)
    NBuffer=256;
  else if(BUFFER_CODE==9)
    NBuffer=512;
  else if(BUFFER_CODE==10)
    NBuffer=1024;
  else{
    std::cout<<"This is not a valid BUFFER_CODE choice"<<std::endl;
    NBuffer=-1;
  }

  std::cout<<"NBuffer : "<<NBuffer<<std::endl;
  std::cout<<"MSize : "<<MSize<<std::endl;

  int Length = MSize/NBuffer;

  std::cout<<"Length : "<<Length<<std::endl;

  return Length;

}

int GetData( sis3153eth* crate, UINT base_address, std::vector<UINT>& data, UINT nwords){

  data.clear();

  for(int i=0; i<nwords; i++){

    //std::cout<<"reading : "<<i<<std::endl;

    UINT addr = base_address;
    UINT chData;
    UINT return_code = crate->vme_A32D32_read (addr, &chData); //

    // ToDo - check error return code and throw exception

    data.push_back( chData );

  }

  return 0;
}

int GetHeader( sis3153eth* crate, UINT base_address, std::vector<UINT>& channel_mask, UINT& event_size ){

  // get relevant information for parsing the event
  std::vector< UINT > header;
  GetData(crate, base_address, header, 4);

  std::cout<<"HeaderSize : "<<header.size()<<std::endl;
  for(int j=0; j<(int)header.size(); j++){
    printf("Header:  \t\taddr = 0x%08X    \tdata = 0x%02X \n", j, header.at(j));
  }

  // TODO : put some checks and printout for the header info
  // are the first few bits 0101?

  // get the event size for this read
  // this includes the 4 bytes for the header so we remove them
  event_size = (header.at(0) & 0x0FFFFFF) - 4; // recover the last bits that are the event size using a mask

  // save the channel masks
  channel_mask.push_back( (header.at(1) & 0x01) >> 0 );
  channel_mask.push_back( (header.at(1) & 0x02) >> 1 );
  channel_mask.push_back( (header.at(1) & 0x04) >> 2 );
  channel_mask.push_back( (header.at(1) & 0x08) >> 3 );
  channel_mask.push_back( (header.at(1) & 0x10) >> 4 );
  channel_mask.push_back( (header.at(1) & 0x20) >> 5 );
  channel_mask.push_back( (header.at(1) & 0x40) >> 6 );
  channel_mask.push_back( (header.at(1) & 0x80) >> 7 );

  return 0;
}

int GetEventData( sis3153eth* crate, UINT base_address, std::vector< std::vector< UINT > >& parsed_channel_data, std::vector<UINT> channel_mask, UINT event_size, UINT buffer_size, UINT n_chan ){

  std::vector< UINT > rawChannelData;
  GetData(crate, base_address, rawChannelData, event_size);

  std::cout<<"RawData : "<<rawChannelData.size()<<std::endl;
  for(int j=0; j<(int)rawChannelData.size(); j++){
    printf("Data:  \t\taddr = 0x%08X    \tdata = 0x%02X \n", j, rawChannelData.at(j));
  }

  // TODO - put some checks that the number of channels and the buffer length makes sense

  // now parse the event data buffer depending on the channel mask info
  UINT currentLoc=0;

  std::vector< std::vector< UINT > > parsedChannelData;

  for(int iChan=0; iChan<n_chan; iChan++){

    std::cout<<"Location : "<<iChan<<std::endl;

    // this is where we will read to
    UINT stopLoc = currentLoc + buffer_size/2;

    // check if we are out of range
    if( stopLoc>rawChannelData.size() ){
      std::cout<<"YOU are looking outside of your bounds"<<std::endl;
      // TODO : throw an exception
    }


    parsed_channel_data.push_back({});

    if(channel_mask.at(iChan)==1){
      std::cout<<"Channel good to read : "<<iChan<<" start = "<<currentLoc<<"  stop = "<<stopLoc<<std::endl;
      for(int iDat=currentLoc; iDat<stopLoc; iDat++){

        // two readings are stored in one word
        UINT chData = rawChannelData.at(iDat);

        UINT ev0    = (chData & 0xFFFF0000) >> 16;  // first half of event doublet
        UINT ev1    = (chData & 0x0000FFFF);        // second half of event doublet

        parsed_channel_data.at(iChan).push_back( ev1 );
        parsed_channel_data.at(iChan).push_back( ev0 );

      }
      currentLoc = stopLoc;
    }

  }

  return 0;
}

int WriteOutput( std::vector< std::vector< UINT > > channel_data, std::vector< UINT > channel_mask, UINT buffer_size, UINT n_chan, string outputname){

  std::fstream fout;
  fout.open(outputname.c_str(), std::ios::out);

  for(int iData=0; iData<buffer_size; iData++){

    std::cout<<"Writing out : "<<iData<<std::endl;

    // reading value
    fout<<setw(5)<<iData<<"   ";

    // write data for each channel that is active and -1 if its not active
    for(int iChan=0; iChan<n_chan; iChan++){
      if(channel_mask.at(iChan)==1){
        fout<<setw(10)<<channel_data.at(iChan).at(iData)<<"   ";
      }
      else{
        fout<<setw(10)<<"-1"<<"   ";
      }
    }

    // write a newline
    fout<<"\n";

  }

  fout.close();

  return 0;
}

int SendSoftwareTrigger( sis3153eth* crate, UINT base_address, UINT& n_events_in_buffer, UINT& event_size, UINT& readout_status, UINT& acquisition_status){

  UINT addr = base_address + 0x8108;
  UINT data = 0x1;
  UINT return_code = crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"\n\n[] Post-Trigger Info"<<std::endl;
  std::cout<<"NEvents"<<std::endl;
  addr = base_address + 0x812C;
  return_code = crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Event Size"<<std::endl;
  addr = base_address + 0x814C;
  return_code = crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status"<<std::endl;
  addr = base_address + 0xEF04;
  return_code = crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status Acquisition"<<std::endl;
  addr = base_address + 0x8104;
  return_code = crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  return 0;
}

int main(int argc, char *argv[])
{

  unsigned int addr;
  unsigned int data ;
  unsigned int i ;
  unsigned int request_nof_words ;
  unsigned int got_nof_words ;
  unsigned int written_nof_words ;

  unsigned char uchar_data  ;
  unsigned short ushort_data  ;
  std::cout << "sis3153eth_access_test" << std::endl; // prints sis3316_access_test_sis3153eth

  //char char_command[256];
  char  ip_addr_string[32] ;
  unsigned int vme_base_address ;
  char ch_string[64] ;
  int int_ch ;
  int return_code ;

// #ifdef ETHERNET_UDP_INTERFACE
//   char  pc_ip_addr_string[32] ;
//   strcpy(sis3316_ip_addr_string,"128.141.48.76") ; // SIS3316 IP address
// #endif


  // default
  vme_base_address = 0x00320000;
  strcpy(ip_addr_string,"128.141.48.76") ; // SIS3153 IP address

     if (argc > 1) {
  #ifdef raus
       /* Save command line into string "command" */
       memset(char_command,0,sizeof(char_command));
       for (i=1;i<argc;i++) {
        strcat(char_command,argv[i]);
        strcat(char_command," ");
      }
      printf("gl_command %s    \n", char_command);
  #endif


      while ((int_ch = getopt(argc, argv, "?hI:")) != -1)
        switch (int_ch) {
          case 'I':
            sscanf(optarg,"%s", ch_string) ;
            printf("-I %s    \n", ch_string );
            strcpy(ip_addr_string,ch_string) ;
            break;
          case 'X':
          sscanf(optarg,"%X", &vme_base_address) ;
          break;
          case '?':
          case 'h':
          default:
            printf("   \n");
          printf("Usage: %s  [-?h] [-I ip]  ", argv[0]);
          printf("   \n");
          printf("   \n");
            printf("   -I string     SIS3153 IP Address       	Default = %s\n", ip_addr_string);
          printf("   \n");
          printf("   -h            Print this message\n");
          printf("   \n");
          exit(1);
          }

     } // if (argc > 1)
    printf("\n");


  // open the vme crate connection
  sis3153eth *vme_crate;
  sis3153eth(&vme_crate, ip_addr_string);

  char char_messages[128] ;
  unsigned int nof_found_devices ;

  return_code = vme_crate->vmeopen ();  // open Vme interface
  vme_crate->get_vmeopen_messages (char_messages, &nof_found_devices);  // open Vme interface
  printf("get_vmeopen_messages = %s , nof_found_devices %d \n",char_messages, nof_found_devices);


  // read the basic configuration information of the interface board
  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Testing interface card configurations"<<std::endl;
  std::cout<<"====================================="<<std::endl;
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_CONTROL_STATUS, &data); //
  printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_MODID_VERSION, &data); //
  printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_MODID_VERSION, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_SERIAL_NUMBER_REG, &data); //
  printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_SERIAL_NUMBER_REG, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_LEMO_IO_CTRL_REG, &data); //
  printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_LEMO_IO_CTRL_REG, data,return_code);

  // turn the LED A on and off to demonstrate functionality of communication
  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Testing interface card communications with LED A on and off"<<std::endl;
  std::cout<<"====================================="<<std::endl;
  usleep(100000);
  data = 1<<0;
  return_code = vme_crate->udp_sis3153_register_write (addr, data); //
  printf("udp_sis3153_register_write: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data,return_code);
  usleep(100000);

  addr = SIS3153USB_CONTROL_STATUS;
  data = 1<<16;
  return_code = vme_crate->udp_sis3153_register_write (addr, data); //
  printf("udp_sis3153_register_write: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data,return_code);

  usleep(100000);
  data = 1<<0;
  return_code = vme_crate->udp_sis3153_register_write (addr, data); //
  printf("udp_sis3153_register_write: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data,return_code);

  usleep(100000);

  std::cout<<"The light should still be on for LED A on the interface card"<<std::endl;




  std::cout<<"VME Control Before"<<std::endl;
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_VME_MASTER_CONTROL_STATUS, &data); //
  printf("SIS3153USB_VME_MASTER_CONTROL_STATUS: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_VME_MASTER_CONTROL_STATUS, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_VME_MASTER_CYCLE_STATUS, &data); //
  printf("SIS3153USB_VME_MASTER_CYCLE_STATUS:   addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_VME_MASTER_CYCLE_STATUS, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_VME_INTERRUPT_STATUS, &data); //
  printf("SIS3153USB_VME_INTERRUPT_STATUS:      addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_VME_INTERRUPT_STATUS, data,return_code);


  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Testing reading/writing to digitizer through some address"<<std::endl;
  std::cout<<"====================================="<<std::endl;




  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Testing ROM memory access"<<std::endl;
  std::cout<<"====================================="<<std::endl;

  // loaded ROM memory
  addr = 0x0032F000;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \tshould be checksum : checksum is unique to the board \n", addr, data,return_code);

  addr = 0x0032F030;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \tshould be vers     : 0x30 \n", addr, data,return_code);

  addr = 0x0032F034;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \tshould be board2   : 0x00 \n", addr, data,return_code);

  addr = 0x0032F038;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \tshould be board1   : 0x06 \n", addr, data,return_code);

  addr = 0x0032F03C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \tshould be board0   : 0xB8 \n", addr, data,return_code);

  addr = 0x0032F010;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \tshould be check   : 0x83 \n", addr, data,return_code);


  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Testing r/w of SCRATCH register"<<std::endl;
  std::cout<<"====================================="<<std::endl;

  addr = 0x0032EF20;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  addr = 0x0032EF20;
  data = 0x01010101;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  addr = 0x0032EF20;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);





  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"CONFIGURE"<<std::endl;
  std::cout<<"====================================="<<std::endl;


  std::cout<<"\n\n[] Check Firmware"<<std::endl;
  addr = 0x00328124;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);


  for(int i=0; i<8; i++){
    addr = 0x0032108C + (0x0100 * i);
    return_code = vme_crate->vme_A32D32_read (addr, &data); //
    printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
  }


  // perform system reset
  std::cout<<"\n\n[] Reset to clear system state"<<std::endl;
  std::cout<<"System configs"<<std::endl;
  addr = 0x0032EF24;
  data = 0x1111;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Memory buffer"<<std::endl;
  addr = 0x0032EF28;
  data = 0x1111;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"\n\n[] Reset Pre-Trigger Info"<<std::endl;
  std::cout<<"NEvents"<<std::endl;
  addr = 0x0032812C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Event Size"<<std::endl;
  addr = 0x0032814C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);




  std::cout<<"\n\n[] Check the buffer length"<<std::endl;
  std::cout<<"Table"<<std::endl;
  addr = 0x0032800C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Unique"<<std::endl;
  addr = 0x00328020;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);




  std::cout<<"\n\n[] Modify buffer pre-sample length we use"<<std::endl;
  std::cout<<"PRE Check1"<<std::endl;
  addr = 0x0032800C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"PRE Set"<<std::endl;
  addr = 0x0032800C;
  data = 0x0A;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"PRE Check2"<<std::endl;
  addr = 0x0032800C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  UINT BufferLength = GetBufferLength(data);
  std::cout<<"Buffer Length : "<<BufferLength<<std::endl;




  std::cout<<"POST Check1"<<std::endl;
  addr = 0x00328114;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"POST Set"<<std::endl;
  addr = 0x00328114;
  data = 0x200;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"POST Check2"<<std::endl;
  addr = 0x00328114;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
  std::cout<<"PostTriggerSamp : "<<data<<std::endl;



  std::cout<<"\n\n[] Global Trigger Mask"<<std::endl;
  addr = 0x0032810C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);





  std::cout<<"\n\n[] Start sawtooth waveform"<<std::endl;

  addr = 0x00328000;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  addr = 0x00328000;
  data = 0x18; // 18 is important to keep that funky bit set
  //data = 0x10; // no signal - see noise
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  addr = 0x00328000;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);


  usleep(100000);


  std::cout<<"\n\n[] Check channel enable"<<std::endl;
  std::cout<<"Check channel enable mask : Before"<<std::endl;
  addr = 0x00328120;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Set channel enable mask"<<std::endl;
  addr = 0x00328120;
  data = 0x00320000 + (0<<7) + (0<<6) + (0<<5) + (0<<4) + (0<<3) + (0<<2) + (1<<1) + (1<<0);
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Check channel enable mask : After"<<std::endl;
  addr = 0x00328120;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);



  std::cout<<"\n\n[] Check channel DC offset"<<std::endl;
  addr = 0x00321098;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
  addr = 0x00321198;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
  addr = 0x00321298;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
  addr = 0x00321398;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
  addr = 0x00321498;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
  addr = 0x00321598;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
  addr = 0x00321698;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
  addr = 0x00321798;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);




  std::cout<<"\n\n[] Read Event"<<std::endl;
  std::cout<<"NMaxEvents to read for configuring block transfer"<<std::endl;
  addr = 0x0032EF1C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  addr = 0x0032EF1C;
  data = 0x01;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  addr = 0x0032EF1C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);


  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"START RUNNING"<<std::endl;
  std::cout<<"====================================="<<std::endl;


  std::cout<<"\n\n[] Check acquisition settings "<<std::endl;
  std::cout<<"Check acquisition"<<std::endl;
  addr = 0x00328100;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Start Acquisition"<<std::endl;
  addr = 0x00328100;
  data = 0x04;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Check acquisition"<<std::endl;
  addr = 0x00328100;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);



  usleep(10000);




  std::cout<<"\n\n[] Pre-Trigger Info"<<std::endl;
  std::cout<<"NEvents"<<std::endl;
  addr = 0x0032812C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Event Size"<<std::endl;
  addr = 0x0032814C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status"<<std::endl;
  addr = 0x0032EF04;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status Acquisition"<<std::endl;
  addr = 0x00328104;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status"<<std::endl;
  addr = 0x0032EF04;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  usleep(100000);

  std::cout<<"\n\n==========================="<<std::endl;
  std::cout<<"Sending triggers"<<std::endl;
  std::cout<<"==========================="<<std::endl;
  UINT nEvents=0;
  UINT thisEventSize=0;
  UINT readoutStatus;
  UINT acquisitionStatus;

  int x_in;
  std::cout<<"Waiting for input"<<std::endl;
  std::cin>>x_in;


  SendSoftwareTrigger( vme_crate, vme_base_address, nEvents, thisEventSize, readoutStatus, acquisitionStatus);

/*
  addr = 0x00328108;
  data = 0x1;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"\n\n[] Post-Trigger Info"<<std::endl;
  std::cout<<"NEvents"<<std::endl;
  addr = 0x0032812C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Event Size"<<std::endl;
  addr = 0x0032814C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status"<<std::endl;
  addr = 0x0032EF04;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status Acquisition"<<std::endl;
  addr = 0x00328104;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);
*/


  ////////////////////////////////////////////////////////////////////////////////////
  // read header
  ////////////////////////////////////////////////////////////////////////////////////
  std::vector< UINT > channelMask;
  UINT eventSize;
  GetHeader(vme_crate, vme_base_address, channelMask, eventSize);

  ////////////////////////////////////////////////////////////////////////////////////
  // read data
  ////////////////////////////////////////////////////////////////////////////////////
  UINT nChan=8;
  std::vector< std::vector< UINT > > parsedChannelData;
  GetEventData( vme_crate, vme_base_address, parsedChannelData, channelMask, eventSize, BufferLength, nChan );

  ////////////////////////////////////////////////////////////////////////////////////
  // write data to output file
  ////////////////////////////////////////////////////////////////////////////////////

  std::cout<<"\nChecking Channel Data"<<std::endl;
  for(int iChan=0; iChan<nChan; iChan++){
    std::cout<<"Channel : "<<iChan<<"  -  Size : "<<parsedChannelData.at(iChan).size()<<std::endl;
  }


  WriteOutput(parsedChannelData, channelMask, BufferLength, nChan, "testOutput.txt");







  std::cout<<"\n\n[] Post-ReadOut Info"<<std::endl;
  std::cout<<"NEvents"<<std::endl;
  addr = 0x0032812C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Event Size"<<std::endl;
  addr = 0x0032814C;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status"<<std::endl;
  addr = 0x0032EF04;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status Acquisition"<<std::endl;
  addr = 0x00328104;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);



  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"STOP RUNNING"<<std::endl;
  std::cout<<"====================================="<<std::endl;

  std::cout<<"\n\n[] STOP Check acquisition settings - 0x00 means software trigger ready"<<std::endl;
  std::cout<<"Check acquisition"<<std::endl;
  addr = 0x00328100;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Stop Acquisition"<<std::endl;
  addr = 0x00328100;
  data = 0x00;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Check acquisition"<<std::endl;
  addr = 0x00328100;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);


  return 0;
}































