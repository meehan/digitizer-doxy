//============================================================================
// Name        : sis3153eth_access_test.cpp
// Author      : Sam Meehan
// Version     :
// Copyright   : Your copyright notice
// Description : A test read of the interface card memory
//============================================================================
#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#ifdef ETHERNET_VME_INTERFACE
  #include "sis3153ETH_vme_class.h"
  sis3153eth *gl_vme_crate;
  char  gl_sis3153_ip_addr_string[32] = "128.141.48.76";

  #ifdef LINUX
    #include <sys/types.h>
    #include <sys/socket.h>
  #endif

  #ifdef WINDOWS
  #include <winsock2.h>
  #pragma comment(lib, "ws2_32.lib")
  typedef int socklen_t;
  #endif
#endif

int main(int argc, char *argv[])
{

  unsigned int addr;
  unsigned int data;
  unsigned short ushort_data;
  cout << "sis3153eth_access_test" << endl; // prints sis3316_access_test_sis3153eth

     //char char_command[256];
  char  ip_addr_string[32] ;
  unsigned int vme_base_address ;
  char ch_string[64] ;
  int int_ch ;
  int return_code ;

#ifdef ETHERNET_UDP_INTERFACE
  char  pc_ip_addr_string[32] ;
  strcpy(sis3316_ip_addr_string,"128.141.48.76") ; // SIS3316 IP address
#endif


  // default
  vme_base_address = 0x30000000 ;
  strcpy(ip_addr_string,"128.141.48.76") ; // SIS3153 IP address

     if (argc > 1) {
  #ifdef raus
       /* Save command line into string "command" */
       memset(char_command,0,sizeof(char_command));
       for (i=1;i<argc;i++) {
        strcat(char_command,argv[i]);
        strcat(char_command," ");
      }
      printf("gl_command %s    \n", char_command);
  #endif


      while ((int_ch = getopt(argc, argv, "?hI:")) != -1)
        switch (int_ch) {
          case 'I':
            sscanf(optarg,"%s", ch_string) ;
            printf("-I %s    \n", ch_string );
            strcpy(ip_addr_string,ch_string) ;
            break;
          case 'X':
          sscanf(optarg,"%X", &vme_base_address) ;
          break;
          case '?':
          case 'h':
          default:
            printf("   \n");
          printf("Usage: %s  [-?h] [-I ip]  ", argv[0]);
          printf("   \n");
          printf("   \n");
            printf("   -I string     SIS3153 IP Address       	Default = %s\n", ip_addr_string);
          printf("   \n");
          printf("   -h            Print this message\n");
          printf("   \n");
          exit(1);
          }

     } // if (argc > 1)
    printf("\n");




#ifdef ETHERNET_VME_INTERFACE
  sis3153eth *vme_crate;
  sis3153eth(&vme_crate, ip_addr_string);
#endif

  printf("===================================================\n");
  printf("Playing with LED A :\n");
  printf("===================================================\n");

  char char_messages[128] ;
  unsigned int nof_found_devices ;

  // open Vme Interface device
  return_code = vme_crate->vmeopen();  // open Vme interface
  vme_crate->get_vmeopen_messages (char_messages, &nof_found_devices);  // open Vme interface
  printf("get_vmeopen_messages = %s , nof_found_devices %d \n",char_messages, nof_found_devices);


  // get initial status of the register in the control/status that controls the LED
  return_code = vme_crate->udp_sis3153_register_read(SIS3153USB_CONTROL_STATUS, &data);
  printf("Control Status read : addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data, return_code);


  // set LEDA on
  unsigned int data_write=(1<<0);
  return_code = vme_crate->udp_sis3153_register_write(SIS3153USB_CONTROL_STATUS, data_write);
  printf("Control Status write : addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data_write, return_code);

  // read it again
  return_code = vme_crate->udp_sis3153_register_read(SIS3153USB_CONTROL_STATUS, &data);
  printf("Control Status read : addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data, return_code);

  std::cout<<"Wait for input, once entered, LEDA will go off."<<std::endl;
  int x;
  cin>>x;

  // set LEDA on
  data_write=(1<<16);
  return_code = vme_crate->udp_sis3153_register_write(SIS3153USB_CONTROL_STATUS, data_write);
  printf("Control Status write : addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data_write, return_code);

  // read it again
  return_code = vme_crate->udp_sis3153_register_read(SIS3153USB_CONTROL_STATUS, &data);
  printf("Control Status read : addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data, return_code);


  // now perform a loop and each time turn the LED on and then off
  for(int i=0; i<10; i++){

    std::cout<<"Running step : "<<i<<std::endl;

    std::cout<<"off"<<std::endl;
    vme_crate->udp_sis3153_register_write(SIS3153USB_CONTROL_STATUS, (1<<16) ); // turn off
    usleep(500000);
    std::cout<<"on"<<std::endl;
    vme_crate->udp_sis3153_register_write(SIS3153USB_CONTROL_STATUS, (1<<0) ); // turn on
    usleep(500000);


  }


  printf("\n");




	return 0;
}

