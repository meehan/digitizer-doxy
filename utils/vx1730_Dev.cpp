//============================================================================
// Name        : vx1730_Dev
// Author      : Sam Meehan
// Description : Executable for developing the DAQ readout of vx1730
//============================================================================

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>

#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"

#include "Comm_vx1730.h"

#include "nlohmann/json.hpp"
using json = nlohmann::json;

int main(int argc, char *argv[])
{


  // used to tag the name of the program if you want in the output file
  std::string programName = argv[0];
  std::cout<<"Program Running : "<<programName<<std::endl;
  std::size_t pos = programName.find("/");
  std::string programTag = programName.substr(pos+1);
  std::cout<<"Program Tag     : "<<programTag<<std::endl;

  // require that you point the program at a config file
  // which will at least be used for the ip address and local base address
  if(argc<=1){
    std::cout<<"********************************************"<<std::endl;
    std::cout<<"Incorrect Usage : "<<std::endl;
    std::cout<<"Please check usage via the [-h] help option"<<std::endl;
    std::cout<<"********************************************"<<std::endl;
    return 1;
  }

  // argument parsing to get the path to the config file
  char *cPath = NULL;
  char *oPath = NULL;
  bool forceRemove = false;
  bool debug = false;
  int c;

  while ((c = getopt (argc, argv, "hc:o:f:d:")) != -1)
    switch (c)
      {
      case 'h':
        std::cout<<"\n\nThis is the help screen for your vx1730 digitizer command line running"<<std::endl;
        std::cout<<"You must provide the following arguments : "<<std::endl;
        std::cout<<" -c [CONFIGFILE] : CONFIGFILE is the path to the json formatted config file"<<std::endl;
        std::cout<<" -o [OUTPUTPATH] : OUTPUTPATH is the location of the directory that you wish"<<std::endl;
        std::cout<<"                   to use to store your output files\n\n"<<std::endl;
        return 1;
        break;
      case 'c':
        cPath = optarg;
        break;
      case 'o':
        oPath = optarg;
        break;
      case 'f':
        forceRemove = true;
        break;
      case 'd':
        debug = true;
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument. Path to the config file.\n", optopt);
        if (optopt == 'o')
          fprintf (stderr, "Option -%o requires an argument. Path to the output directory which will be replaced during running.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort();
      }


  // open the json config file
  std::string configPath(cPath);
  std::cout<<"\n\nConfig Path : "<<configPath<<std::endl;
  json myConfig = openJsonFile(configPath.c_str());

  // output directory for running
  std::string outputPath(oPath);
  outputPath += "/";
  std::cout<<"\n\nOutput Path : "<<outputPath<<std::endl;
  int dir_err = 0;

  // if enabled you will remove the output directory to start freshly
  if(forceRemove){
    dir_err = system( (std::string("rm -r ")+outputPath).c_str() );
    if(dir_err == -1){
      std::cout<<"Error removing directory : "<<outputPath<<std::endl;
      return 1;
    }
  }
  
  // creation of output directory
  dir_err = system( (std::string("mkdir -p ")+outputPath).c_str() );
  if(dir_err == -1){
    std::cout<<"Error creating directory : "<<outputPath<<std::endl;
    return 1;
  }

  // ip address
  char  ip_addr_string[32] ;
  strcpy(ip_addr_string, std::string(myConfig["ip"]).c_str() ) ; // SIS3153 IP address
  std::cout<<"\nIP Address : "<<ip_addr_string<<std::endl;

  // vme base address
  std::string vme_base_address_str = std::string(myConfig["vme_base_address"]);
  UINT vme_base_address = std::stoi(vme_base_address_str,0,16);
  std::cout<<"\nBase VME Address = 0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<vme_base_address<<std::endl;

  // make a new digitizer instance
  vx1730 *digitizer = new vx1730(ip_addr_string, vme_base_address);

  // test digitizer board interface
  digitizer->TestComm();



  // the above is necessary configuration - below is where the functionality happens



  
  // configure and start acquisition
  digitizer->DumpEventCount( debug );  
  digitizer->Configure( myConfig, debug);
  digitizer->DumpEventCount( debug );
  digitizer->MonitorTemperature( outputPath+"temperature_0.dat");
  digitizer->StartAcquisition( debug );
  digitizer->MonitorTemperature( outputPath+"temperature_1.dat");
  
  // manual software trigger
  digitizer->DumpEventCount( debug );
  digitizer->SendSWTrigger( debug );
  digitizer->SendSWTrigger( debug );
  digitizer->SendSWTrigger( debug );
  digitizer->SendSWTrigger( debug );
  digitizer->DumpEventCount( debug );
  
  digitizer->DumpFrontEvent( outputPath+"DataSWTrigTest.txt");
  digitizer->DumpEventCount( debug );

  // stop data acquisition
  digitizer->StopAcquisition( debug );
  digitizer->MonitorTemperature( outputPath+"temperature_2.dat");
  digitizer->DumpEventCount( debug );


  // running with self-trigger
  std::cout<<"starting acquisition"<<std::endl;
  digitizer->StartAcquisition( debug );
  digitizer->DumpConfig();

  // make output for this run
  std::string outputRunDirectory = outputPath+"/run_"+GetDateVerboseString()+"/";
  dir_err = system( (std::string("mkdir -p ")+outputRunDirectory).c_str() );
  if(dir_err == -1){
    std::cout<<"Error creating directory : "<<outputRunDirectory<<std::endl;
    return 1;
  }
  
  // event counter for readout
  int counter_output=0;
  
  int nloops=1;
  

  
  for(int iLoop=0; iLoop<nloops; iLoop++){
  
    std::cout<<"NEvents BeforeRead : "<<std::dec<<digitizer->DumpEventCount()<<std::endl;
  
    while(digitizer->DumpEventCount()){
      digitizer->DumpFrontEvent(outputRunDirectory+programTag+"_event_"+to_string(counter_output)+".txt");
      counter_output++;
    }

    std::cout<<"NEvents AfterRead : "<<std::dec<<digitizer->DumpEventCount()<<std::endl;

    Wait(2.0);
  }
  
  digitizer->StopAcquisition( debug );
  
  
  /////////////////////////////////
  // playing with LVDS
  /////////////////////////////////

  std::cout<<"starting acquisition"<<std::endl;
  digitizer->StartAcquisition( debug );
  digitizer->DumpConfig();  
  
// #define VX1730_FP_IO_DATA                      0x8118
// #define VX1730_FP_IO_CONTROL                   0x811C  
  
  unsigned int lvds_data;
  ReadSlaveReg(digitizer->crate, digitizer->base_address+VX1730_FP_IO_DATA, lvds_data);
  std::cout<<"VX1730_FP_IO_DATA (0x8118)    : "<<std::hex<<lvds_data<<std::endl;
  std::cout<<ConvertIntToWord(lvds_data)<<std::endl;

  unsigned int lvds_control;
  ReadSlaveReg(digitizer->crate, digitizer->base_address+VX1730_FP_IO_CONTROL, lvds_control);
  std::cout<<"VX1730_FP_IO_CONTROL (0x811C) : "<<std::hex<<lvds_control<<std::endl;
  std::cout<<ConvertIntToWord(lvds_control)<<std::endl;
  
  SetBit(lvds_control, 0, 1); // LEMO trigger out is TTL (trig high)
  SetBit(lvds_control, 1, 0); // LEMO trigger enable - 0=enabled, 1=disabled 

  SetBit(lvds_control, 2, 1); // LVDS[0,3] set to output 
  SetBit(lvds_control, 3, 1); // LVDS[4,7] set to output 
  SetBit(lvds_control, 4, 0); // LVDS[8,11] set to output 
  SetBit(lvds_control, 5, 0); // LVDS[12,15] set to output 
  
  SetBit(lvds_control, 8, 1); // using fancy-ass new features --> requires configuration of 0x81A0  
  
  WriteSlaveReg(digitizer->crate, digitizer->base_address+VX1730_FP_IO_CONTROL, lvds_control);
  ReadSlaveReg(digitizer->crate, digitizer->base_address+VX1730_FP_IO_CONTROL, lvds_control);
  std::cout<<"VX1730_FP_IO_CONTROL (0x811C) : "<<std::hex<<lvds_control<<std::endl;
  std::cout<<ConvertIntToWord(lvds_control)<<std::endl;  
  
  unsigned int lvds_output_config;
  ReadSlaveReg(digitizer->crate, digitizer->base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  std::cout<<"VX1730_FP_LVDS_IO_CTRL (0x81A0) : "<<std::hex<<lvds_output_config<<std::endl;
  std::cout<<ConvertIntToWord(lvds_output_config)<<std::endl;
  
  SetBit(lvds_output_config, 0, 1);
  SetBit(lvds_output_config, 1, 0);
  SetBit(lvds_output_config, 2, 0);
  SetBit(lvds_output_config, 3, 0);
  
  SetBit(lvds_output_config, 4, 1);
  SetBit(lvds_output_config, 5, 0);
  SetBit(lvds_output_config, 6, 0);
  SetBit(lvds_output_config, 7, 0);  

  WriteSlaveReg(digitizer->crate, digitizer->base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  ReadSlaveReg(digitizer->crate, digitizer->base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  std::cout<<"VX1730_FP_LVDS_IO_CTRL (0x811C) : "<<std::hex<<lvds_output_config<<std::endl;
  std::cout<<ConvertIntToWord(lvds_output_config)<<std::endl;  

  // do a loop with a wait to manually send a trigger bunch 
  nloops=10000;
  for(int iLoop=0; iLoop<nloops; iLoop++){
    std::cout<<"iLoop : "<<iLoop<<std::endl;
    std::cout<<"NEvents BeforeRead : "<<std::dec<<digitizer->DumpEventCount( false )<<std::endl;
    while(digitizer->DumpEventCount( false )){
      digitizer->DumpFrontEvent( outputRunDirectory+programTag+"_event_"+to_string(counter_output)+".txt");
      counter_output++;
    }
    std::cout<<"NEvents AfterRead : "<<std::dec<<digitizer->DumpEventCount( false )<<std::endl;

    Wait(10.0);
  }
  
  
  digitizer->StopAcquisition( debug );



  return 0;
}



