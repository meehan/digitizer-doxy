//============================================================================
// Name        : sis3153eth_access_test.cpp
// Author      : th
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <map>

using namespace std;

#ifdef ETHERNET_VME_INTERFACE
  #include "sis3153ETH_vme_class.h"
  sis3153eth *gl_vme_crate;
  char  gl_sis3153_ip_addr_string[32] = "128.141.206.13";

  #ifdef LINUX
    #include <sys/types.h>
    #include <sys/socket.h>
  #endif

  #ifdef WINDOWS
  #include <winsock2.h>
  #pragma comment(lib, "ws2_32.lib")
  typedef int socklen_t;
  #endif
#endif


unsigned int gl_dma_buffer[0x10000] ;

int main(int argc, char *argv[])
{

  unsigned int addr;
  unsigned int data ;
  unsigned int i ;
  unsigned int request_nof_words ;
  unsigned int got_nof_words ;
  unsigned int written_nof_words ;

  unsigned char uchar_data  ;
  unsigned short ushort_data  ;
  cout << "sis3153eth_access_test" << endl; // prints sis3316_access_test_sis3153eth

  //char char_command[256];
  char  ip_addr_string[32] ;
  unsigned int vme_base_address ;
  char ch_string[64] ;
  int int_ch ;
  int return_code ;

#ifdef ETHERNET_UDP_INTERFACE
  char  pc_ip_addr_string[32] ;
  strcpy(sis3316_ip_addr_string,"128.141.206.13") ; // SIS3316 IP address
#endif


  // default
  vme_base_address = 0x30000000 ;
  strcpy(ip_addr_string,"128.141.206.13") ; // SIS3153 IP address

     if (argc > 1) {
  #ifdef raus
       /* Save command line into string "command" */
       memset(char_command,0,sizeof(char_command));
       for (i=1;i<argc;i++) {
        strcat(char_command,argv[i]);
        strcat(char_command," ");
      }
      printf("gl_command %s    \n", char_command);
  #endif


      while ((int_ch = getopt(argc, argv, "?hI:")) != -1)
        switch (int_ch) {
          case 'I':
            sscanf(optarg,"%s", ch_string) ;
            printf("-I %s    \n", ch_string );
            strcpy(ip_addr_string,ch_string) ;
            break;
          case 'X':
          sscanf(optarg,"%X", &vme_base_address) ;
          break;
          case '?':
          case 'h':
          default:
            printf("   \n");
          printf("Usage: %s  [-?h] [-I ip]  ", argv[0]);
          printf("   \n");
          printf("   \n");
            printf("   -I string     SIS3153 IP Address       	Default = %s\n", ip_addr_string);
          printf("   \n");
          printf("   -h            Print this message\n");
          printf("   \n");
          exit(1);
          }

     } // if (argc > 1)
    printf("\n");




#ifdef ETHERNET_VME_INTERFACE
  sis3153eth *vme_crate;
  sis3153eth(&vme_crate, ip_addr_string);
#endif


  char char_messages[128] ;
  unsigned int nof_found_devices ;

  // open Vme Interface device
  return_code = vme_crate->vmeopen ();  // open Vme interface
  vme_crate->get_vmeopen_messages (char_messages, &nof_found_devices);  // open Vme interface
    printf("get_vmeopen_messages = %s , nof_found_devices %d \n",char_messages, nof_found_devices);


  // read the basic configuration information of the interface board
  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Testing interface card configurations"<<std::endl;
  std::cout<<"====================================="<<std::endl;
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_CONTROL_STATUS, &data); //
  printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_MODID_VERSION, &data); //
  printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_MODID_VERSION, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_SERIAL_NUMBER_REG, &data); //
  printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_SERIAL_NUMBER_REG, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_LEMO_IO_CTRL_REG, &data); //
  printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_LEMO_IO_CTRL_REG, data,return_code);

  // turn the LED A on and off to demonstrate functionality of communication
  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Testing interface card communications with LED A on and off"<<std::endl;
  std::cout<<"====================================="<<std::endl;
  usleep(100000);
  data = 1<<0;
  return_code = vme_crate->udp_sis3153_register_write (addr, data); //
  printf("udp_sis3153_register_write: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data,return_code);
  usleep(100000);

  addr = SIS3153USB_CONTROL_STATUS;
  data = 1<<16;
  return_code = vme_crate->udp_sis3153_register_write (addr, data); //
  printf("udp_sis3153_register_write: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data,return_code);

  usleep(100000);
  data = 1<<0;
  return_code = vme_crate->udp_sis3153_register_write (addr, data); //
  printf("udp_sis3153_register_write: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data,return_code);

  usleep(100000);

  std::cout<<"The light should still be on for LED A on the interface card"<<std::endl;




  std::cout<<"VME Control Before"<<std::endl;
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_VME_MASTER_CONTROL_STATUS, &data); //
  printf("SIS3153USB_VME_MASTER_CONTROL_STATUS: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_VME_MASTER_CONTROL_STATUS, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_VME_MASTER_CYCLE_STATUS, &data); //
  printf("SIS3153USB_VME_MASTER_CYCLE_STATUS:   addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_VME_MASTER_CYCLE_STATUS, data,return_code);
  return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_VME_INTERRUPT_STATUS, &data); //
  printf("SIS3153USB_VME_INTERRUPT_STATUS:      addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_VME_INTERRUPT_STATUS, data,return_code);


  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Testing ROM memory access"<<std::endl;
  std::cout<<"====================================="<<std::endl;

  // loaded ROM memory
  addr = 0x0032F000;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \tshould be checksum : checksum is unique to the board \n", addr, data,return_code);




  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Registers"<<std::endl;
  std::cout<<"====================================="<<std::endl;

  map< std::string , unsigned int > myRegisters;


  myRegisters["ZS Thresh"]= 0x8024;
  myRegisters["ZST 0"]= 0x1024;
  myRegisters["ZST 1"]= 0x1124;
  myRegisters["ZST 2"]= 0x1224;
  myRegisters["ZST 3"]= 0x1324;
  myRegisters["ZST 4"]= 0x1424;
  myRegisters["ZST 5"]= 0x1524;
  myRegisters["ZST 6"]= 0x1624;
  myRegisters["ZST 7"]= 0x1724;

  myRegisters["ZS Sample"]=0x8028;
  myRegisters["ZSS 0"]=0x1028;
  myRegisters["ZSS 1"]=0x1128;
  myRegisters["ZSS 2"]=0x1228;
  myRegisters["ZSS 3"]=0x1328;
  myRegisters["ZSS 4"]=0x1428;
  myRegisters["ZSS 5"]=0x1528;
  myRegisters["ZSS 6"]=0x1628;
  myRegisters["ZSS 7"]=0x1728;

  myRegisters["ChanN Trig Thresh"]=0x8080;
  myRegisters["ChanTrigThresh 0"]=0x1080;
  myRegisters["ChanTrigThresh 1"]=0x1180;
  myRegisters["ChanTrigThresh 2"]=0x1280;
  myRegisters["ChanTrigThresh 3"]=0x1380;
  myRegisters["ChanTrigThresh 4"]=0x1480;
  myRegisters["ChanTrigThresh 5"]=0x1580;
  myRegisters["ChanTrigThresh 6"]=0x1680;
  myRegisters["ChanTrigThresh 7"]=0x1780;

  myRegisters["TOT"]=0x8084;
  myRegisters["TOT0"]=0x1084;
  myRegisters["TOT1"]=0x1184;
  myRegisters["TOT2"]=0x1284;
  myRegisters["TOT3"]=0x1384;
  myRegisters["TOT4"]=0x1484;
  myRegisters["TOT5"]=0x1584;
  myRegisters["TOT6"]=0x1684;
  myRegisters["TOT7"]=0x1784;

  myRegisters["ChanStat0"]=0x1088;
  myRegisters["ChanStat1"]=0x1188;
  myRegisters["ChanStat2"]=0x1288;
  myRegisters["ChanStat3"]=0x1388;
  myRegisters["ChanStat4"]=0x1488;
  myRegisters["ChanStat5"]=0x1588;
  myRegisters["ChanStat6"]=0x1688;
  myRegisters["ChanStat7"]=0x1788;

  myRegisters["AMCFW0"]=0x108C;
  myRegisters["AMCFW1"]=0x118C;
  myRegisters["AMCFW2"]=0x128C;
  myRegisters["AMCFW3"]=0x138C;
  myRegisters["AMCFW4"]=0x148C;
  myRegisters["AMCFW5"]=0x158C;
  myRegisters["AMCFW6"]=0x168C;
  myRegisters["AMCFW7"]=0x178C;

  myRegisters["DCOffset"]=0x8098;
  myRegisters["DCO0"]=0x1098;
  myRegisters["DCO1"]=0x1198;
  myRegisters["DCO2"]=0x1298;
  myRegisters["DCO3"]=0x1398;
  myRegisters["DCO4"]=0x1498;
  myRegisters["DCO5"]=0x1598;
  myRegisters["DCO6"]=0x1698;
  myRegisters["DCO7"]=0x1798;


  myRegisters["BoardConfig"]=0x8000;
  myRegisters["BoardConfig BitSet"]= 0x8004;
  myRegisters["BoardConfig BitClear"]=0x8008;
  myRegisters["Buffer Organiza on"]=                         0x800C;
  myRegisters["Custom Size"]=                                0x8020;
  myRegisters["Acquisi on Control"]=                         0x8100;
  myRegisters["Acquisi on Status"]=                          0x8104;
  myRegisters["So ware Trigger"]=                            0x8108;
  myRegisters["Global Trigger Mask"]=                        0x810C;
  myRegisters["Front Panel TRG-OUT (GPO) Enable Mask"]=      0x8110;
  myRegisters["Post Trigger"]=                               0x8114;
  myRegisters["LVDS I/O Data"]=                              0x8118;
  myRegisters["Front Panel I/O Control"]=                    0x811C;
  myRegisters["Channel Enable Mask"]=                        0x8120;
  myRegisters["ROC FPGA Firmware Revision"]=                 0x8124;
  myRegisters["Event Stored"]=                               0x812C;
  myRegisters["Voltage Level Mode Configura on"]=            0x8138;
  myRegisters["So ware Clock Sync"]=                         0x813C;
  myRegisters["Board Info"]=                                 0x8140;
  myRegisters["Analog Monitor Mode"]=                        0x8144;
  myRegisters["Event Size"]=                                 0x814C;
  myRegisters["Fan Speed Control"]=                          0x8168;
  myRegisters["Memory Buffer Almost Full Level"]=            0x816C;
  myRegisters["Run/Start/Stop Delay"]=                       0x8170;
  myRegisters["Board Failure Status"]=                       0x8178;
  myRegisters["Front Panel LVDS I/O New Features"]=          0x81A0;
  myRegisters["Buffer Occupancy Gain"]=                      0x81B4;
  myRegisters["Extended Veto Delay"]=                        0x81C4;
  myRegisters["Readout Control"]=                            0xEF00;
  myRegisters["Readout Status "]=                            0xEF04;
  myRegisters["Board ID"]=                                   0xEF08;
  myRegisters["MCST Base Address and Control"]=              0xEF0C;
  myRegisters["Reloca on Address"]=                          0xEF10;
  myRegisters["Interrupt Status/ID"]=                        0xEF14;
  myRegisters["Interrupt Event Number"]=                     0xEF18;
  myRegisters["Max Number of Events per BLT"]=               0xEF1C;
  myRegisters["Scratch"]=                                    0xEF20;
  myRegisters["So ware Reset"]=                              0xEF24;
  myRegisters["So ware Clear"]=                              0xEF28;
  myRegisters["Configura on Reload"]=                        0xEF34;
  myRegisters["Configura on ROM Checksum"]=                  0xF000;
  myRegisters["Configura on ROM Checksum Length BYTE 2"]=    0xF004;
  myRegisters["Configura on ROM Checksum Length BYTE 1"]=    0xF008;
  myRegisters["Configura on ROM Checksum Length BYTE 0"]=    0xF00C;
  myRegisters["Configura on ROM Constant BYTE 2"]=           0xF010;
  myRegisters["Configura on ROM Constant BYTE 1"]=           0xF014;
  myRegisters["Configura on ROM Constant BYTE 0"]=           0xF018;
  myRegisters["Configura on ROM C Code"]=                    0xF01C;
  myRegisters["Configura on ROM R Code"]=                    0xF020;
  myRegisters["Configura on ROM IEEE OUI BYTE 2"]=           0xF024;
  myRegisters["Configura on ROM IEEE OUI BYTE 1"]=           0xF028;
  myRegisters["Configura on ROM IEEE OUI BYTE 0"]=           0xF02C;
  myRegisters["Configura on ROM Board Version"]=             0xF030;
  myRegisters["Configura on ROM Board Form Factor"]=         0xF034;
  myRegisters["Configura on ROM Board ID BYTE 1"]=           0xF038;
  myRegisters["Configura on ROM Board ID BYTE 0"]=           0xF03C;
  myRegisters["Configura on ROM PCB Revision BYTE 3"]=       0xF040;
  myRegisters["Configura on ROM PCB Revision BYTE 2"]=       0xF044;
  myRegisters["Configura on ROM PCB Revision BYTE 1"]=       0xF048;
  myRegisters["Configura on ROM PCB Revision BYTE 0"]=       0xF04C;
  myRegisters["Configura on ROM FLASH Type"]=                0xF050;
  myRegisters["Configura on ROM Board Serial Number BYTE 1"]=0xF080;
  myRegisters["Configura on ROM Board Serial Number BYTE 0"]=0xF084;
  myRegisters["Configura on ROM VCXO Type"]=                 0xF088;


  // loop over registers;
  map<std::string, unsigned int>::iterator it;


  ofstream out0;
  out0.open("state0.txt");

  for ( it = myRegisters.begin(); it != myRegisters.end(); it++ ){
    addr = (0x00320000) + it->second;
    return_code = vme_crate->vme_A32D32_read (addr, &data);
    printf("addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", addr, data, return_code);
    out0<<"addr = 0x"<<setw(8)<<std::hex<<addr<<"    data = 0x"<<setw(8)<<std::hex<<std::setfill('0')<<data<<"    return_code = "<<setw(8)<<std::hex<<std::setfill('0')<<return_code<<"\n";
  }

  out0.close();




  // perform system reset
  addr = 0x0032EF24;
  data = 0x1111;
  return_code = vme_crate->vme_A32D32_write (addr, data); //
  printf("vme_A32D32_write:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);





  ofstream out1;
  out1.open("state1.txt");

  for ( it = myRegisters.begin(); it != myRegisters.end(); it++ ){
    addr = (0x00320000) + it->second;
    return_code = vme_crate->vme_A32D32_read (addr, &data);
    printf("addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", addr, data, return_code);
    out1<<"addr = 0x"<<setw(8)<<std::hex<<addr<<"    data = 0x"<<setw(8)<<std::hex<<std::setfill('0')<<data<<"    return_code = "<<setw(8)<<std::hex<<std::setfill('0')<<return_code<<"\n";
  }

  out1.close();

  return 0;
}



























































