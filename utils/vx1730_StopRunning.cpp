//============================================================================
// Name        : sis3153eth_access_test.cpp
// Author      : th
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <ctime>


#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"
#include "Helper_vx1730.h"

using namespace std;

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include <chrono>
using namespace std::chrono;





int main(int argc, char *argv[])
{

  // these are variables that should be controlled by DAQ
  uint32_t source_id = 0;
  uint64_t event_id  = 0;

  // require that you point the program at a config file
  if(argc<=1){
    std::cout<<"********************************************"<<std::endl;
    std::cout<<"Incorrect Usage : "<<std::endl;
    std::cout<<"Please check usage via the [-h] help option"<<std::endl;
    std::cout<<"********************************************"<<std::endl;
    return 1;
  }

  // argument parsing to get the path to the config file
  char *ipAddress   = NULL;
  char *baseAddress = NULL;
  int index;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "hi:b:")) != -1)
    switch (c)
      {
      case 'h':
        std::cout<<"\n\nThis is the help screen for your vx1730 digitizer command line running"<<std::endl;
        std::cout<<"You must provide the following arguments : "<<std::endl;
        std::cout<<" -c [CONFIGFILE] : CONFIGFILE is the path to the json formatted config file"<<std::endl;
        std::cout<<" -o [OUTPUTPATH] : OUTPUTPATH is the location of the directory that you wish"<<std::endl;
        std::cout<<"                   to use to store your output files\n\n"<<std::endl;
        return 1;
        break;
      case 'i':
        ipAddress = optarg;
        break;
      case 'b':
        baseAddress = optarg;
        break;
      case '?':
        if (optopt == 'i')
          fprintf (stderr, "Option -i requires an argument. Path to the config file.\n");
        if (optopt == 'b')
          fprintf (stderr, "Option -b requires an argument. Path to the output directory which will be replaced during running.\n");
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort();
      }


  // open the json config file
  std::cout<<"\n ipAddress   : "<<ipAddress<<std::endl;
  std::cout<<"\n baseAddress : "<<baseAddress<<std::endl;



  // ip address
  char  ip_addr_string[32] ;
  strcpy(ip_addr_string, std::string(ipAddress).c_str() ) ; // SIS3153 IP address
  std::cout<<"\nIP Address : "<<ip_addr_string<<std::endl;

  // vme base address
  std::string vme_base_address_str = std::string(baseAddress);
  UINT vme_base_address = std::stoi(vme_base_address_str,0,16);
  std::cout<<"\nBase VME Address = 0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<vme_base_address<<std::endl;





  /////////////////////////////////
  // open the vme crate connection
  /////////////////////////////////
  sis3153eth *vme_crate;
  sis3153eth(&vme_crate, ip_addr_string);

  int return_code;
  char char_messages[128] ;
  unsigned int nof_found_devices ;

  return_code = vme_crate->vmeopen();  // open Vme interface
  vme_crate->get_vmeopen_messages(char_messages, &nof_found_devices);  // open Vme interface
  std::cout<<"\n\nOpenning VME connection : "<<std::endl;
  std::cout<<"get_vmeopen_messages = "<<setw(20)<<char_messages<<"nof_found_devices"<<nof_found_devices<<std::endl;

  /////////////////////////////////
  // test interface of boards
  /////////////////////////////////
  sis3153_TestComm( vme_crate, true );
  vx1730_TestComm( vme_crate, vme_base_address, true );

  /////////////////////////////////
  // stop after reseting buffer memory
  /////////////////////////////////
  
  vx1730_DumpConfig( vme_crate, vme_base_address );
  vx1730_DumpEventCount( vme_crate, vme_base_address, true );
  vx1730_Reset( vme_crate, vme_base_address, true );
  vx1730_DumpEventCount( vme_crate, vme_base_address, true );
  vx1730_StopAcquisition( vme_crate, vme_base_address,true );


  return 0;
}



