//============================================================================
// Name        : sis3153eth_access_test.cpp
// Author      : th
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <sstream>
#include <cstdlib>
/*!
# MAIN TEST PRIOGRAM
*/

#include <ctime>


#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"
#include "Helper_vx1730.h"

using namespace std;

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include <chrono>
using namespace std::chrono;



int main(int argc, char *argv[])
{

  // these are variables that should be controlled by DAQ
  uint32_t source_id = 0;
  uint64_t event_id  = 0;

  std::string programName = argv[0];

  // require that you point the program at a config file
  if(argc<=1){
    std::cout<<"********************************************"<<std::endl;
    std::cout<<"Incorrect Usage : "<<std::endl;
    std::cout<<"Please check usage via the [-h] help option"<<std::endl;
    std::cout<<"********************************************"<<std::endl;
    return 1;
  }

  // argument parsing to get the path to the config file
  char *cPath = NULL;
  char *oPath = NULL;
  bool forceRemove = false;
  int index;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "hc:o:f:")) != -1)
    switch (c)
      {
      case 'h':
        std::cout<<"\n\nThis is the help screen for your vx1730 digitizer command line running"<<std::endl;
        std::cout<<"You must provide the following arguments : "<<std::endl;
        std::cout<<" -c [CONFIGFILE] : CONFIGFILE is the path to the json formatted config file"<<std::endl;
        std::cout<<" -o [OUTPUTPATH] : OUTPUTPATH is the location of the directory that you wish"<<std::endl;
        std::cout<<"                   to use to store your output files\n\n"<<std::endl;
        return 1;
        break;
      case 'c':
        cPath = optarg;
        break;
      case 'o':
        oPath = optarg;
        break;
      case 'f':
        forceRemove = true;
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument. Path to the config file.\n", optopt);
        if (optopt == 'o')
          fprintf (stderr, "Option -%o requires an argument. Path to the output directory which will be replaced during running.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort();
      }


  // open the json config file
  std::string configPath(cPath);
  std::cout<<"\n\nConfig Path : "<<configPath<<std::endl;
  json myConfig = openJsonFile(configPath.c_str());

  // output directory for running
  std::string outputPath(oPath);
  outputPath += "/";
  std::cout<<"\n\nOutput Path : "<<outputPath<<std::endl;
  int dir_err = 0;

  if(forceRemove){
    dir_err = system( (std::string("rm -r ")+outputPath).c_str() );
    if(dir_err == -1){
      std::cout<<"Error removing directory : "<<outputPath<<std::endl;
      return 1;
    }
  }
  
  dir_err = system( (std::string("mkdir -p ")+outputPath).c_str() );
  if(dir_err == -1){
    std::cout<<"Error creating directory : "<<outputPath<<std::endl;
    return 1;
  }


  // ip address
  char  ip_addr_string[32] ;
  strcpy(ip_addr_string, std::string(myConfig["ip"]).c_str() ) ; // SIS3153 IP address
  std::cout<<"\nIP Address : "<<ip_addr_string<<std::endl;

  // vme base address
  std::string vme_base_address_str = std::string(myConfig["vme_base_address"]);
  UINT vme_base_address = std::stoi(vme_base_address_str,0,16);
  std::cout<<"\nBase VME Address = 0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<vme_base_address<<std::endl;

  // debug flag
  bool debug = myConfig["debug"];
  std::cout<<"\ndebug flag : "<<debug<<std::endl;





  /////////////////////////////////
  // open the vme crate connection
  /////////////////////////////////
  sis3153eth *vme_crate;
  sis3153eth(&vme_crate, ip_addr_string);

  int return_code;
  char char_messages[128] ;
  unsigned int nof_found_devices ;

  return_code = vme_crate->vmeopen();  // open Vme interface
  vme_crate->get_vmeopen_messages(char_messages, &nof_found_devices);  // open Vme interface
  std::cout<<"\n\nOpenning VME connection : "<<std::endl;
  std::cout<<"get_vmeopen_messages = "<<setw(20)<<char_messages<<"nof_found_devices"<<nof_found_devices<<std::endl;


  /////////////////////////////////
  // test interface of boards
  /////////////////////////////////
  sis3153_TestComm( vme_crate, debug );
  vx1730_TestComm( vme_crate, vme_base_address, debug );

  /////////////////////////////////
  // configure and start acquisition
  /////////////////////////////////
  
  vx1730_DumpEventCount( vme_crate, vme_base_address, debug );
  
  vx1730_Configure( vme_crate, vme_base_address, myConfig, debug);
  
  vx1730_DumpEventCount( vme_crate, vme_base_address, debug );

  vx1730_MonitorTemperature( vme_crate, vme_base_address, outputPath+"temperature_0.dat");

  vx1730_StartAcquisition( vme_crate, vme_base_address, debug );

  vx1730_MonitorTemperature( vme_crate, vme_base_address, outputPath+"temperature_1.dat");



  /////////////////////////////////
  // manual software trigger
  /////////////////////////////////
  vx1730_DumpEventCount( vme_crate, vme_base_address, debug );
  vx1730_SendSWTrigger( vme_crate, vme_base_address, debug );
  vx1730_SendSWTrigger( vme_crate, vme_base_address, debug );
  vx1730_SendSWTrigger( vme_crate, vme_base_address, debug );
  vx1730_SendSWTrigger( vme_crate, vme_base_address, debug );
  vx1730_DumpEventCount( vme_crate, vme_base_address, debug );
  

  /////////////////////////////////
  // get the event
  /////////////////////////////////
  uint32_t raw_payload[MAXFRAGSIZE];
  vx1730_ReadRawEvent( vme_crate, vme_base_address, raw_payload, debug );

  std::cout<<"Print the header manually for debugging"<<std::endl;
  for(int iWord=0; iWord<4; iWord++){
    std::cout<<"Word : "<<iWord<<"  "<<raw_payload[iWord]<<std::endl;
  }

  Payload_Dump( raw_payload );

  vx1730_DumpEventCount( vme_crate, vme_base_address, debug );

  /////////////////////////////////
  // format the event by putting the FASER header on it
  /////////////////////////////////
  int payload_size = Payload_GetEventSize( raw_payload );
  const int total_size = sizeof(EventFragmentHeader) + sizeof(uint32_t) * payload_size;

  std::cout<<"Raw Payload size           : "<<std::dec<<payload_size<<std::endl;
  std::cout<<"Total EventFragment size   : "<<std::dec<<total_size<<std::endl;

  // event fragment creation
  std::unique_ptr<EventFragment>data((EventFragment *)malloc(total_size));

  // formatting event to include FASER header
  microseconds timestamp;
  timestamp = duration_cast<microseconds>(system_clock::now().time_since_epoch());


  // header
  // FIXME : need to make this a real header
  data->header.marker         = FragmentMarker;
  data->header.fragment_tag   = PhysicsTag;
  data->header.trigger_bits   = 0;
  data->header.version_number = EventFragmentVersion;
  data->header.header_size    = sizeof(data->header);
  data->header.payload_size   = payload_size;
  data->header.source_id      = event_id;
  data->header.event_id       = source_id;
  data->header.bc_id          = 0xFFFF;
  data->header.status         = 0;
  data->header.timestamp      = event_id;

  // raw data payload
  unsigned int ncopy = payload_size * 4;
  std::cout<<"Making payload copy : "<<std::dec<<ncopy<<std::endl;
  memcpy(data->payload, raw_payload, ncopy);


  /////////////////////////////////
  // check that it copied by dumping again
  /////////////////////////////////
  Data_Dump( data, 5, debug );

  std::string outputfile = outputPath+"Data.dat";

  Data_Write( data, outputfile);


  /////////////////////////////////
  // STOP data acquisition
  /////////////////////////////////
  vx1730_StopAcquisition( vme_crate, vme_base_address, debug );
  vx1730_MonitorTemperature( vme_crate, vme_base_address, outputPath+"temperature_2.dat");
  vx1730_DumpEventCount( vme_crate, vme_base_address, debug );






  /////////////////////////////////
  // running with self-trigger
  /////////////////////////////////

  std::cout<<"starting acquisition"<<std::endl;
  vx1730_StartAcquisition( vme_crate, vme_base_address, debug );
  vx1730_DumpConfig( vme_crate, vme_base_address );

  // make output for this run
  std::string outputRunDirectory = outputPath+"/run_"+GetDateVerboseString()+"/";
  dir_err = system( (std::string("mkdir -p ")+outputRunDirectory).c_str() );
  if(dir_err == -1){
    std::cout<<"Error creating directory : "<<outputRunDirectory<<std::endl;
    return 1;
  }
  
  // event counter for readout
  int counter_output=0;
  
  int nloops=1;
  
  std::cout<<"Program : "<<programName<<std::endl;
  
  std::size_t pos = programName.find("/");
  std::string programTag = programName.substr(pos+1);

  std::cout<<"ProgramTag : "<<programTag<<std::endl;
  
  for(int iLoop=0; iLoop<nloops; iLoop++){
  
    std::cout<<"NEvents BeforeRead : "<<std::dec<<vx1730_DumpEventCount( vme_crate, vme_base_address, false )<<std::endl;
  
    while(vx1730_DumpEventCount( vme_crate, vme_base_address, false )){
      vx1730_DumpFrontEvent( vme_crate, vme_base_address, outputRunDirectory+programTag+"_event_"+to_string(counter_output)+".txt");
      counter_output++;
    }

    std::cout<<"NEvents AfterRead : "<<std::dec<<vx1730_DumpEventCount( vme_crate, vme_base_address, false )<<std::endl;

    Wait(2.0);
  }
  
  vx1730_StopAcquisition( vme_crate, vme_base_address, debug );
  
  
  /////////////////////////////////
  // playing with LVDS
  /////////////////////////////////

  std::cout<<"starting acquisition"<<std::endl;
  vx1730_StartAcquisition( vme_crate, vme_base_address, debug );
  vx1730_DumpConfig( vme_crate, vme_base_address );  

  unsigned int lvds_control;
  ReadSlaveReg(vme_crate, vme_base_address+VX1730_FP_IO_CONTROL, lvds_control);
  std::cout<<"VX1730_FP_IO_CONTROL (0x811C) : "<<std::hex<<lvds_control<<std::endl;
  std::cout<<ConvertIntToWord(lvds_control)<<std::endl;
  
  SetBit(lvds_control, 0, 1); // LEMO trigger out is TTL (trig high)
  SetBit(lvds_control, 1, 0); // LEMO trigger enable - 0=enabled, 1=disabled 

  SetBit(lvds_control, 2, 1); // LVDS[0,3] set to output 
  SetBit(lvds_control, 3, 1); // LVDS[4,7] set to output 
  SetBit(lvds_control, 4, 0); // LVDS[8,11] set to output 
  SetBit(lvds_control, 5, 0); // LVDS[12,15] set to output 
  
  SetBit(lvds_control, 8, 1); // using fancy-ass new features --> requires configuration of 0x81A0  
  
  WriteSlaveReg(vme_crate, vme_base_address+VX1730_FP_IO_CONTROL, lvds_control);
  ReadSlaveReg(vme_crate, vme_base_address+VX1730_FP_IO_CONTROL, lvds_control);
  std::cout<<"VX1730_FP_IO_CONTROL (0x811C) : "<<std::hex<<lvds_control<<std::endl;
  std::cout<<ConvertIntToWord(lvds_control)<<std::endl;  
  
  unsigned int lvds_output_config;
  ReadSlaveReg(vme_crate, vme_base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  std::cout<<"VX1730_FP_LVDS_IO_CTRL (0x81A0) : "<<std::hex<<lvds_output_config<<std::endl;
  std::cout<<ConvertIntToWord(lvds_output_config)<<std::endl;
  
  SetBit(lvds_output_config, 0, 1);
  SetBit(lvds_output_config, 1, 0);
  SetBit(lvds_output_config, 2, 0);
  SetBit(lvds_output_config, 3, 0);
  
  SetBit(lvds_output_config, 4, 1);
  SetBit(lvds_output_config, 5, 0);
  SetBit(lvds_output_config, 6, 0);
  SetBit(lvds_output_config, 7, 0);  

  WriteSlaveReg(vme_crate, vme_base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  ReadSlaveReg(vme_crate, vme_base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  std::cout<<"VX1730_FP_LVDS_IO_CTRL (0x811C) : "<<std::hex<<lvds_output_config<<std::endl;
  std::cout<<ConvertIntToWord(lvds_output_config)<<std::endl;  

  // do a loop with a wait to manually send a trigger bunch 
  nloops=10000;
  for(int iLoop=0; iLoop<nloops; iLoop++){
    std::cout<<"iLoop : "<<iLoop<<std::endl;
    std::cout<<"NEvents BeforeRead : "<<std::dec<<vx1730_DumpEventCount( vme_crate, vme_base_address, false )<<std::endl;
    while(vx1730_DumpEventCount( vme_crate, vme_base_address, false )){
      vx1730_DumpFrontEvent( vme_crate, vme_base_address, outputRunDirectory+programTag+"_event_"+to_string(counter_output)+".txt");
      counter_output++;
    }
    std::cout<<"NEvents AfterRead : "<<std::dec<<vx1730_DumpEventCount( vme_crate, vme_base_address, false )<<std::endl;

    Wait(1.0);
  }
  
  
  vx1730_StopAcquisition( vme_crate, vme_base_address, debug );

  return 0;
}



