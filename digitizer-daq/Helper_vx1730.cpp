//============================================================================
// Name        : DigitizerHelper
// Author      : Sam Meehan
// Version     : v0.0
// Copyright   : Your copyright notice
// Description : Helper functions for the FASER digitizer readout.
//============================================================================


#include "Helper_vx1730.h"


int vx1730_TestComm( sis3153eth* crate, UINT base_address, bool debug ){
  std::cout<<"\n\n[]   vx1730_TestComm"<<std::endl;

  int return_code;
  unsigned int data;
  unsigned int addr;

  std::cout<<"\nScratch test"<<std::endl;

  // testing scratch space
  ReadSlaveReg(crate, base_address+VX1730_SCRATCH, data );
  WriteSlaveReg(crate, base_address+VX1730_SCRATCH, 0x22222222 );
  ReadSlaveReg(crate, base_address+VX1730_SCRATCH, data );
  WriteSlaveReg(crate, base_address+VX1730_SCRATCH, 0x10101010 );
  ReadSlaveReg(crate, base_address+VX1730_SCRATCH, data );

  // testing scratch space on each channel
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, data );
  WriteSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, 0x55555555 );
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, data );
  WriteSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, 0x30303030 );
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, data );


  std::cout<<"\nTesting ROM memory access"<<std::endl;

  // three registers that should be fixed
  ReadSlaveReg(crate, base_address+VX1730_CONFIG_ROM, data );
  ReadSlaveReg(crate, base_address+VX1730_CONFIG_ROM_BOARD_VERSION, data );
  ReadSlaveReg(crate, base_address+VX1730_CONFIG_ROM_BOARD_FORMFACTOR, data );

  std::cout<<"\nCheck Fixed Configurations"<<std::endl;

  std::cout<<"Global"<<std::endl;
  ReadSlaveReg(crate, base_address+VX1730_ROC_FPGA_FW_REV, data );

  std::cout<<"Channel-by-Channel"<<std::endl;
  for(unsigned int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_AMC_FIRMWARE_VERSION+(0x0100*iChan), data );
  }

  return 0;
}

int vx1730_Configure( sis3153eth* crate, UINT base_address, json config, bool debug){
  std::cout<<"\n\n[] vx1730_Configure"<<std::endl;

  int return_code;
  unsigned int data;
  unsigned int addr;

  // perform system reset

  std::cout<<"\nReset : System configurations"<<std::endl;
  WriteSlaveReg(crate, base_address+VX1730_SW_RESET, 0x1111, debug );


  std::cout<<"\nReset : Clear memory buffers"<<std::endl;
  WriteSlaveReg(crate, base_address+VX1730_SW_CLEAR, 0x1111, debug );


  std::cout<<"\nBuffer Organization : Number of buffers = "<<config["buffer_length"]<<std::endl;
  WriteSlaveReg(crate, base_address+VX1730_BUFFER_ORGANIZATION, std::stoi( std::string(config["buffer_length"]),0,16), debug );


  std::cout<<"\nBuffer Organization : Post-sample length = "<<config["buffer_n_post_samples"]<<std::endl;
  WriteSlaveReg(crate, base_address+VX1730_POST_TRIGGER_SETTING, std::stoi( std::string(config["buffer_n_post_samples"]),0,16), debug );


  std::cout<<"\nTrigger Global Sources"<<std::endl;  
  unsigned int trig_enable_mask;
  ReadSlaveReg(crate, base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );

  std::cout<<"Initial TrigEnableMask : "<<std::hex<<trig_enable_mask<<std::endl;

  // set all trigger couples to 0
  trig_enable_mask &= ~(1<<0);
  trig_enable_mask &= ~(1<<2);
  trig_enable_mask &= ~(1<<3);
  trig_enable_mask &= ~(1<<4);
  trig_enable_mask &= ~(1<<5);
  trig_enable_mask &= ~(1<<6);
  trig_enable_mask &= ~(1<<7);

  // enable each group separately
  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){
    int group  = (int)config["channel_config"].at(iGrp)["group"];
    int enable = (int)config["channel_config"].at(iGrp)["trig_enable"];
    trig_enable_mask |= (enable << group);
  }

  std::cout<<"LVDS     : "<<(int)config["global_trigger"]["lvds"]     << std::endl;
  std::cout<<"External : "<<(int)config["global_trigger"]["external"] << std::endl;
  std::cout<<"Software : "<<(int)config["global_trigger"]["software"] << std::endl;

  // change the global features
  trig_enable_mask &= ~(1<<29);
  trig_enable_mask |= ( (int)config["global_trigger"]["lvds"]     << 29 );
  trig_enable_mask &= ~(1<<30);
  trig_enable_mask |= ( (int)config["global_trigger"]["external"] << 30 );
  trig_enable_mask &= ~(1<<31);
  trig_enable_mask |= ( (int)config["global_trigger"]["software"] << 31 );

  std::cout<<"Final TrigEnableMask : "<<std::hex<<trig_enable_mask<<std::endl;

  WriteSlaveReg(crate, base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  ReadSlaveReg(crate, base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  
  
  std::cout<<"\nTrigger Group Logic and Output"<<std::endl;    
  
  // enable each group separately
  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){
    int group  = (int)config["channel_config"].at(iGrp)["group"];
    
    std::cout<<"Group : "<<group<<"  "<<std::string(config["channel_config"].at(iGrp)["trig_logic"])<<std::endl;
    
    unsigned int trig_chan_logic;
    ReadSlaveReg(crate, base_address + VX1730_CHANNEL_TRIG_LOGIC + (0x0100)*group, trig_chan_logic, true);
    std::cout<<"ChanLogic Init : "<<trig_chan_logic<<std::endl;
    
    if( std::string(config["channel_config"].at(iGrp)["trig_logic"]).compare("AND")==0 ){
      SetBit(trig_chan_logic,0,0);
      SetBit(trig_chan_logic,1,0);
    }
    else if( std::string(config["channel_config"].at(iGrp)["trig_logic"]).compare("OnlyA")==0 ){
      SetBit(trig_chan_logic,0,1);
      SetBit(trig_chan_logic,1,0);
    }
    else if( std::string(config["channel_config"].at(iGrp)["trig_logic"]).compare("OnlyB")==0 ){
      SetBit(trig_chan_logic,0,0);
      SetBit(trig_chan_logic,1,1);
    }
    else if( std::string(config["channel_config"].at(iGrp)["trig_logic"]).compare("OR")==0 ){
      SetBit(trig_chan_logic,0,1);
      SetBit(trig_chan_logic,1,1);
    } 
    else{
        //TODO : throw an exception for improper config
        std::cout<<"This is not a proper trigger logic configuration"<<std::endl;
        exit(18); // is this even a good return code to choose
    }
    
    
    
    std::cout<<"ChanLogic Final : "<<trig_chan_logic<<std::endl;
    WriteSlaveReg(crate, base_address + VX1730_CHANNEL_TRIG_LOGIC + (0x0100)*group, trig_chan_logic, true);
    
    
    
  }
  
  
  
  
  
  
  
  std::cout<<"\nTrigger SelfTrigger Thresholds"<<std::endl;
  
  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){
  
    std::cout<<"\nGroup : "<<iGrp<<"  "<<config["channel_config"].at(iGrp)["group"]<<std::endl;
  
    int channel = 0;
    int threshold = 0;
    unsigned int threshold_bits = 0;
    
    // set channel A
    channel   = (int)config["channel_config"].at(iGrp)["chanA"]["channel"];
    
    std::cout<<"chanA : "<<std::dec<<channel<<"  "<<threshold_bits<<std::endl;
    ReadSlaveReg(crate,  base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold_bits, true);
    WriteSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, std::stoi( std::string(config["channel_config"].at(iGrp)["chanA"]["trig_threshold"]),0,16), true);
    ReadSlaveReg(crate,  base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold_bits, true);
    std::cout<<"Check thresh : "<<threshold_bits<<std::endl;
    
    // set channel B
    channel   = (int)config["channel_config"].at(iGrp)["chanB"]["channel"];
    std::cout<<"chanB : "<<std::dec<<channel<<"  "<<threshold_bits<<std::endl;
    ReadSlaveReg(crate,  base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold_bits, true);
    WriteSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, std::stoi( std::string(config["channel_config"].at(iGrp)["chanB"]["trig_threshold"]),0,16), true);
    ReadSlaveReg(crate,  base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold_bits, true);
    std::cout<<"Check thresh : "<<threshold_bits<<std::endl;
  }
  

  std::cout<<"\nTrigger SelfTrigger Polarity (and test form input [not a trigger])"<<std::endl;
  
  unsigned int channel_config;
  ReadSlaveReg(crate, base_address + VX1730_CHANNEL_CONFIG, channel_config, true );

  std::cout<<"Initial ChannelConfig : "<<std::hex<<channel_config<<std::endl;

  std::cout<<"\nSawTooth Enable : "<<config["internal_sawtooth"]<<std::endl;
  unsigned int sawtooth_enable;
  if(config["internal_sawtooth"]==true){
    std::cout<<"Turning on internal sawtooth waveform"<<std::endl;
    sawtooth_enable = (1<<3); // 18 is important to keep that funky bit set
  }
  else if(config["internal_sawtooth"]==false){
    std::cout<<"No internal sawtooth waveform"<<std::endl;
    sawtooth_enable = (0<<3); // no signal - see noise
  }  
  channel_config |= sawtooth_enable;

  int polarity=0; //0=positive , 1=negative
  if(config["global_trigger"]["polarity"]=="under"){
    polarity=1;
  }
  channel_config |= (polarity << 6);

  std::cout<<"Final ChannelConfig : "<<std::hex<<channel_config<<std::endl;

  WriteSlaveReg(crate, base_address + VX1730_CHANNEL_CONFIG, channel_config, true );
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_CONFIG, data, debug );
  
  std::cout<<"\nTrigger : TrigOut Enable Mask"<<std::endl;
  
  unsigned int trig_out_enable_mask = 0;
  ReadSlaveReg(crate, base_address+VX1730_FP_TRIGGER_OUT_EN_MASK, trig_out_enable_mask, true );
  
  SetBit(trig_out_enable_mask,0,1);
  SetBit(trig_out_enable_mask,29,0);
  SetBit(trig_out_enable_mask,30,0);
  SetBit(trig_out_enable_mask,31,0);
  
  WriteSlaveReg(crate, base_address+VX1730_FP_TRIGGER_OUT_EN_MASK, trig_out_enable_mask, true );
  
  ReadSlaveReg(crate, base_address+VX1730_FP_TRIGGER_OUT_EN_MASK, trig_out_enable_mask, true );
  
  std::cout<<"TrigOutEnable : "<<std::hex<<trig_out_enable_mask<<std::endl;
  
  
  std::cout<<"\nChannel Readout Enabling"<<std::endl;
  
  unsigned int data_enable_mask = 0x0000;
  std::cout<<"DataEnableMask Init  : "<<std::hex<<data_enable_mask<<std::endl;
  
  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){
  
    std::cout<<"\nGroup : "<<iGrp<<"  "<<config["channel_config"].at(iGrp)["group"]<<std::endl;
  
    int channel = 0;
    int readout = 0;
    
    // set channel A
    channel = (int)config["channel_config"].at(iGrp)["chanA"]["channel"];
    readout = (int)config["channel_config"].at(iGrp)["chanA"]["enable_read"];
    std::cout<<"chanA : "<<channel<<"  "<<readout<<std::endl;
    data_enable_mask |= (readout<<channel); // enable if the bit tells us to do so
    
    // set channel B
    channel = (int)config["channel_config"].at(iGrp)["chanB"]["channel"];
    readout = (int)config["channel_config"].at(iGrp)["chanB"]["enable_read"];
    std::cout<<"chanB : "<<channel<<"  "<<readout<<std::endl;
    data_enable_mask |= (readout<<channel); // enable if the bit tells us to do so
    
  }
  
  // writing enable mask
  WriteSlaveReg(crate, base_address + VX1730_CHANNEL_EN_MASK, data_enable_mask, debug );
  ReadSlaveReg(crate, base_address + VX1730_CHANNEL_EN_MASK, data_enable_mask, debug );
  
  std::cout<<"DataEnableMask Final : "<<std::hex<<data_enable_mask<<std::endl;

  
  std::cout<<"\nChannel DC Offsets"<<std::endl;
  
  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){
  
    std::cout<<"Group : "<<iGrp<<"  "<<config["channel_config"].at(iGrp)["group"]<<std::endl;
  
    int channel = 0;
    int dc_off  = 0;
    unsigned int dc_offset_bits;
    
    // set channel A
    std::cout<<"chanA"<<std::endl;
    channel = (int)config["channel_config"].at(iGrp)["chanA"]["channel"];
    dc_off  = (int)config["channel_config"].at(iGrp)["chanA"]["dc_offset"];
    dc_offset_bits = (unsigned int)(floor((dc_off + 1.0)/2.0 * 65536));
    WriteSlaveReg(crate, base_address+VX1730_CHANNEL_DAC+(0x0100*channel), dc_offset_bits, true );
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DAC+(0x0100*channel), dc_offset_bits, true );

    // set channel B
    std::cout<<"chanB"<<std::endl;
    channel = (int)config["channel_config"].at(iGrp)["chanB"]["channel"];
    dc_off  = (int)config["channel_config"].at(iGrp)["chanB"]["dc_offset"];
    dc_offset_bits = (unsigned int)(floor((dc_off + 1.0)/2.0 * 65536));
    WriteSlaveReg(crate, base_address+VX1730_CHANNEL_DAC+(0x0100*channel), dc_offset_bits, true );
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DAC+(0x0100*channel), dc_offset_bits, true );
    
  }
  
  
  
  std::cout<<"\nReadout BLT Size (NEvents/Transfer)"<<std::endl;
  
  // writing number of events to read with block transfer
  WriteSlaveReg(crate, base_address + VX1730_BLT_EVENT_NB, 0x01, debug );

  // check the channel enable mask
  ReadSlaveReg(crate, base_address + VX1730_BLT_EVENT_NB, data, debug );
  
  return 0;
}





int vx1730_Reset( sis3153eth* crate, UINT base_address, bool debug){
  std::cout<<"\n\n[] vx1730_Reset"<<std::endl;

  int return_code;
  unsigned int data;
  unsigned int addr;

  // perform system reset

  std::cout<<"\nReset : System configurations"<<std::endl;
  WriteSlaveReg(crate, base_address+VX1730_SW_RESET, 0x1111, debug );

  std::cout<<"\nReset : Clear memory buffers"<<std::endl;
  WriteSlaveReg(crate, base_address+VX1730_SW_CLEAR, 0x1111, debug );

  return 0;
}


int vx1730_StartAcquisition( sis3153eth* crate, UINT base_address, bool debug ){
  std::cout<<"\n\n[] vx1730_StartAcquisition"<<std::endl;

  unsigned int data;

  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, data, debug );
  WriteSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, 0x04, debug );
  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, data, debug );

  // sleep for a short while before data taking starts
  usleep(10000);

  return 0;

}

int vx1730_StopAcquisition( sis3153eth* crate, UINT base_address, bool debug ){
  std::cout<<"\n\n[] vx1720_StopAcquisition"<<std::endl;

  unsigned int data;

  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, data, debug );
  WriteSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, 0x00, debug );
  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, data, debug );

  return 0;
}

int vx1730_DumpEventCount( sis3153eth* crate, UINT base_address, bool debug ){
  std::cout<<"\n\n[] vx1730_DumpEventCount"<<std::endl;

  unsigned int n_events_in_buffer;
  ReadSlaveReg(crate, base_address + VX1730_EVENT_STORED, n_events_in_buffer, debug );

  unsigned int ev_size;
  ReadSlaveReg(crate, base_address + VX1730_EVENT_SIZE, ev_size, debug );

  unsigned int vme_status;
  ReadSlaveReg(crate, base_address + VX1730_VME_STATUS, vme_status, debug );

  unsigned int acquisition_status;
  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_STATUS, acquisition_status, debug );

  return n_events_in_buffer;
}

int vx1730_SendSWTrigger( sis3153eth* crate, UINT base_address, bool debug ){
  std::cout<<"\n\n[] vx1730_SendSWTrigger"<<std::endl;

  WriteSlaveReg(crate, base_address + VX1730_SW_TRIGGER, 0x1, debug );

  return 0;
}

int vx1730_ReadRawEvent( sis3153eth* crate, UINT base_address, uint32_t raw_payload[], bool debug ){
  std::cout<<"\n\n[] vx1730_ReadRawEvent"<<std::endl;

  unsigned int addr;
  unsigned int data_nevents;
  unsigned int data_eventsize;
  unsigned int data_bltread;
  int return_code;

  // get the number of events
  ReadSlaveReg(crate, base_address + VX1730_EVENT_STORED, data_nevents, debug );
  ReadSlaveReg(crate, base_address + VX1730_EVENT_SIZE, data_eventsize, debug );
  ReadSlaveReg(crate, base_address + VX1730_BLT_EVENT_NB, data_bltread, debug );

  std::cout<<"NEvents in Buffer : "<<std::dec<<data_nevents<<std::endl;
  std::cout<<"Event Size        : "<<std::dec<<data_eventsize<<std::endl;
  std::cout<<"BLT N Read        : "<<std::dec<<data_bltread<<std::endl;

  // read out event in its entirety using block read
  UINT gl_dma_buffer[MAXFRAGSIZE];
  UINT request_nof_words;
  UINT got_nof_words;

  // the size of the read will be the size of the event
  request_nof_words = data_eventsize;

  // initialize full internal buffer to 0
  for(int iWord=0; iWord<request_nof_words; iWord++) {
    gl_dma_buffer[iWord] = 0 ;
  }

  // perform block read
  return_code = crate->vme_A32BLT32_read (base_address + 0x0000, gl_dma_buffer, request_nof_words, &got_nof_words); //
  printf("vme_A32BLT32_read:  \t\taddr = 0x%08X    \tgot_nof_words = 0x%08X \treturn_code = 0x%08X \n", addr, got_nof_words,return_code);

  // TODO : put a check that the number of requested words is the same as the number of gotten words

  // copy the output of the BLT read to the payload
  for(int iWord=0; iWord<request_nof_words; iWord++) {
    std::cout<<"Word : "<<std::setw(5)<<std::dec<<iWord<<"  0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<gl_dma_buffer[iWord]<<std::endl;
    raw_payload[iWord]=gl_dma_buffer[iWord];
  }


//  raw_payload = gl_dma_buffer;

  return 0;

}

int vx1730_MonitorTemperature( sis3153eth* crate, UINT base_address, std::string outputfile, bool debug){
  std::cout<<"\n\n[] vx1730_MonitorTemp"<<std::endl;

  fstream fout;
  fout.open(outputfile, ios::out);

  // write data for each channel that is active and -1 if its not active
  fout<<setw(10)<<"reading";
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    std::string line = "chan-"+to_string(iChan);
    fout<<setw(10)<<line;
  }
  fout<<"\n";

  unsigned int data;
  int temperature;

  // TODO : Make the number of readouts configurable

  for(int iRead=0; iRead<30; iRead++){
    fout<<setw(10)<<std::dec<<iRead;
    for(int iChan=0; iChan<NCHANNELS; iChan++){
      ReadSlaveReg(crate, base_address + VX1730_CHANNEL_TEMPERATURE + 0x0100*iChan, data, debug );
      temperature = data & 0x000000FF;

      //std::cout<<iRead<<"-"<<iChan<<" : "<<std::hex<<data<<"  "<<std::dec<<temperature<<std::endl;

      fout<<setw(10)<<std::dec<<temperature;

    }
    //fout<<std::endl;

    usleep(10000);
  }

  fout.close();

  return 1;
}

int vx1730_GetBufferLength( unsigned int code ){

  int n_samples_per_buffer = 0;

  if(code==0x0)
    n_samples_per_buffer = 639990;
  else if(code==0x1)
    n_samples_per_buffer = 319990;
  else if(code==0x2)
    n_samples_per_buffer = 159990;
  else if(code==0x3)
    n_samples_per_buffer = 79990;
  else if(code==0x4)
    n_samples_per_buffer = 39990;
  else if(code==0x5)
    n_samples_per_buffer = 19990;
  else if(code==0x6)
    n_samples_per_buffer = 9990;
  else if(code==0x7)
    n_samples_per_buffer = 4990;
  else if(code==0x8)
    n_samples_per_buffer = 2550;
  else if(code==0x9)
    n_samples_per_buffer = 1270;
  else if(code==0xA)
    n_samples_per_buffer = 630;

  return n_samples_per_buffer;
}

int vx1730_DumpConfig( sis3153eth* crate, UINT base_address ){
  std::cout<<"\n\n[] vx1730_DumpConfig"<<std::endl;

  // reuse this same value for reading
  unsigned int data;

  // global information
  ReadSlaveReg(crate, base_address+VX1730_BUFFER_ORGANIZATION, data);
  std::cout<<"BUFFER_ORGANIZATION  ("<<std::hex<<VX1730_BUFFER_ORGANIZATION<<") [hex]: 0x"<<std::hex<<data<<"  -  nsamples="<<std::dec<<vx1730_GetBufferLength(data)<<std::endl;

  ReadSlaveReg(crate, base_address+VX1730_POST_TRIGGER_SETTING, data);
  std::cout<<"POST_TRIGGER_SETTING ("<<std::hex<<VX1730_POST_TRIGGER_SETTING<<") [dec]: "<<std::hex<<data<<std::endl;

  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_CONFIG, data);
  std::cout<<"CHANNEL_CONFIG       ("<<std::hex<<VX1730_CHANNEL_CONFIG<<") [bit]: "<<std::hex<<ConvertIntToWord(data)<<std::endl;

  // readout info
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_EN_MASK, data);
  std::cout<<"CHANNEL_EN_MASK      ("<<std::hex<<VX1730_CHANNEL_EN_MASK<<") [bit]: "<<std::hex<<data<<std::endl;

  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DAC+(0x0100*iChan), data);
    std::cout<<"CHANNEL_DAC "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_DAC+(0x0100*iChan)<<") [hex]: "<<std::hex<<data<<std::endl;
  }

  // trigger info
  ReadSlaveReg(crate, base_address+VX1730_TRIG_SRCE_EN_MASK, data);
  std::cout<<"TRIG_SRCE_EN_MASK       ("<<std::hex<<VX1730_TRIG_SRCE_EN_MASK<<") [hex]: "<<std::hex<<data<<std::endl;

  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_THRESH+(0x0100*iChan), data);
    std::cout<<"TRIGGER_THRESH "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_TRIG_THRESH+(0x0100*iChan)<<") [hex]: "<<std::hex<<data<<std::endl;
  }

  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*iChan), data);
    std::cout<<"TRIGGER_PULSE_WIDTH "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*iChan)<<") [hex]: "<<std::hex<<data<<std::endl;
  }
}

int vx1730_DumpFrontEvent( sis3153eth* crate, UINT base_address, std::string outputfile, bool debug ){

  // FIXME : these are variables that should be controlled by DAQ
  uint32_t source_id = 0;
  uint64_t event_id  = 0;

  /////////////////////////////////
  // get the event
  /////////////////////////////////
  uint32_t raw_payload[MAXFRAGSIZE];
  vx1730_ReadRawEvent( crate, base_address, raw_payload, debug );

  std::cout<<"Print the header manually for debugging"<<std::endl;
  for(int iWord=0; iWord<4; iWord++){
    std::cout<<"Word : "<<iWord<<"  "<<raw_payload[iWord]<<std::endl;
  }

  Payload_Dump( raw_payload );

  vx1730_DumpEventCount( crate, base_address, debug );

  /////////////////////////////////
  // format the event by putting the FASER header on it
  /////////////////////////////////
  int payload_size = Payload_GetEventSize( raw_payload );
  const int total_size = sizeof(EventFragmentHeader) + sizeof(uint32_t) * payload_size;

  std::cout<<"Raw Payload size           : "<<std::dec<<payload_size<<std::endl;
  std::cout<<"Total EventFragment size   : "<<std::dec<<total_size<<std::endl;

  // event fragment creation
  std::unique_ptr<EventFragment>data((EventFragment *)malloc(total_size));

  // formatting event to include FASER header
  microseconds timestamp;
  timestamp = duration_cast<microseconds>(system_clock::now().time_since_epoch());


  // header
  // FIXME : need to make this a real header
  data->header.marker         = FragmentMarker;
  data->header.fragment_tag   = PhysicsTag;
  data->header.trigger_bits   = 0;
  data->header.version_number = EventFragmentVersion;
  data->header.header_size    = sizeof(data->header);
  data->header.payload_size   = payload_size;
  data->header.source_id      = event_id;
  data->header.event_id       = source_id;
  data->header.bc_id          = 0xFFFF;
  data->header.status         = 0;
  data->header.timestamp      = event_id;

  // raw data payload
  unsigned int ncopy = payload_size * 4;
  std::cout<<"Making payload copy : "<<std::dec<<ncopy<<std::endl;
  memcpy(data->payload, raw_payload, ncopy);


  /////////////////////////////////
  // check that it copied by dumping again
  /////////////////////////////////
  Data_Dump( data, 5, debug );

  Data_Write( data, outputfile);

  return 0;
}
