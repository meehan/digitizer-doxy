//============================================================================
// Name        : DigitizerHelper
// Author      : Sam Meehan
// Version     : v0.0
// Copyright   : Your copyright notice
// Description : Helper functions for the FASER digitizer readout
//============================================================================

#ifndef  HELPER_SIS3153_INCLUDE_H
#define  HELPER_SIS3153_INCLUDE_H

#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cmath>

#include "Registers_sis3153.h"

using namespace std;

// helper functions
void ReadMasterReg( sis3153eth* crate, unsigned int addr, unsigned int& data, bool debug=false);
void WriteMasterReg( sis3153eth* crate, unsigned int addr, unsigned int data, bool debug=false);
void ReadSlaveReg( sis3153eth* crate, unsigned int addr, unsigned int& data, bool debug=false);
void WriteSlaveReg( sis3153eth* crate, unsigned int addr, unsigned int data, bool debug=false);

// interface card
int sis3153_TestComm( sis3153eth* crate, bool debug=false );

#endif


