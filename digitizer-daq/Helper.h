//============================================================================
// Name        : DigitizerHelper
// Author      : Sam Meehan
// Version     : v0.0
// Copyright   : Your copyright notice
// Description : Helper functions for the FASER digitizer readout
//============================================================================

#ifndef  HELPER_INCLUDE_H
#define  HELPER_INCLUDE_H

#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cmath>

#include <ctime>
#include <chrono>
using namespace std::chrono;

using namespace std;

#include "nlohmann/json.hpp"
// for convenience
using json = nlohmann::json;



void printHelp();

json openJsonFile(std::string filepath);

std::string ConvertIntToWord(unsigned int word, bool debug=false);

void SetBitToZero(unsigned int& word, int bitval);

void SetBitToOne(unsigned int& word, int bitval);

void SetBit(unsigned int& word, int bit_location, int bit_val);

std::string GetDateVerboseString();

void Wait(float nseconds);

#endif




