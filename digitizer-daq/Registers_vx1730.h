//VX1730 digitizer registers

#ifndef  REGISTERS_VX1730_INCLUDE_H
#define  REGISTERS_VX1730_INCLUDE_H

#define VX1730_EVENT_READOUT_BUFFER            0x0000

#define VX1730_CHANNEL_DUMMY32                 0x1024    /* For channel 0 */
#define VX1730_CHANNEL_AMC_FIRMWARE_VERSION    0x108C    /* For channel 0 */
#define VX1730_CHANNEL_DAC                     0x1098    /* For channel 0 */
#define VX1730_CHANNEL_TEMPERATURE             0x10A8    /* For channel 0 */
#define VX1730_CHANNEL_TRIG_THRESH             0x1080    /* For channel 0 */
#define VX1730_CHANNEL_TRIG_PULSE_WIDTH        0x1070    /* For channel 0 */
#define VX1730_CHANNEL_TRIG_LOGIC              0x1084    /* For channel 0 */

#define VX1730_CHANNEL_CONFIG                  0x8000
#define VX1730_CHANNEL_CFG_BIT_SET             0x8004
#define VX1730_CHANNEL_CFG_BIT_CLR             0x8008
#define VX1730_BUFFER_ORGANIZATION             0x800C
#define VX1730_CUSTOM_SIZE                     0x8020
#define VX1730_ACQUISITION_CONTROL             0x8100
#define VX1730_ACQUISITION_STATUS              0x8104
#define VX1730_SW_TRIGGER                      0x8108
#define VX1730_TRIG_SRCE_EN_MASK               0x810C
#define VX1730_FP_TRIGGER_OUT_EN_MASK          0x8110
#define VX1730_POST_TRIGGER_SETTING            0x8114
#define VX1730_FP_IO_DATA                      0x8118
#define VX1730_FP_IO_CONTROL                   0x811C
#define VX1730_CHANNEL_EN_MASK                 0x8120
#define VX1730_ROC_FPGA_FW_REV                 0x8124
#define VX1730_EVENT_STORED                    0x812C
#define VX1730_SET_MONITOR_DAC                 0x8138
#define VX1730_BOARD_INFO                      0x8140
#define VX1730_MONITOR_MODE                    0x8144
#define VX1730_EVENT_SIZE                      0x814C
#define VX1730_FP_LVDS_IO_CTRL                 0x81A0
#define VX1730_ALMOST_FULL_LEVEL               0x816C
#define VX1730_VME_CONTROL                     0xEF00
#define VX1730_VME_STATUS                      0xEF04
#define VX1730_BOARD_ID                        0xEF08
#define VX1730_MULTICAST_BASE_ADDCTL           0xEF0C
#define VX1730_RELOC_ADDRESS                   0xEF10
#define VX1730_INTERRUPT_STATUS_ID             0xEF14
#define VX1730_INTERRUPT_EVT_NB                0xEF18
#define VX1730_BLT_EVENT_NB                    0xEF1C
#define VX1730_SCRATCH                         0xEF20
#define VX1730_SW_RESET                        0xEF24
#define VX1730_SW_CLEAR                        0xEF28
#define VX1730_CONFIG_RELOAD                   0xEF34
#define VX1730_CONFIG_ROM                      0xF000
#define VX1730_CONFIG_ROM_BOARD_VERSION        0xF030
#define VX1730_CONFIG_ROM_BOARD_FORMFACTOR     0xF034



#endif