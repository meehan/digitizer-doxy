#include "Helper_sis3153.h"

void ReadMasterReg( sis3153eth* crate, unsigned int addr, unsigned int& data, bool debug){
  int return_code;
  unsigned int data_read;

  // FIXME : This is a strange hack to get to read out - simple c++ that needs fixing
  return_code = crate->udp_sis3153_register_read (addr, &data_read);
  data = data_read;

  if(debug){
    printf("ReadMasterReg :  \t\taddr = 0x%08X    \tdata = 0x%08X    \treturn_code = 0x%08X \n", addr, data_read, return_code);
  }

  // TODO : add exception for a bad return value
}

void WriteMasterReg( sis3153eth* crate, unsigned int addr, unsigned int data, bool debug){
  int return_code;
  return_code = crate->udp_sis3153_register_write (addr, data);
  if(debug){
    printf("WriteMasterReg :  \t\taddr = 0x%08X    \tdata = 0x%08X    \treturn_code = 0x%08X \n", addr, data, return_code);
  }

  // TODO : add exception for a bad return value
}

void ReadSlaveReg( sis3153eth* crate, unsigned int addr, unsigned int& data, bool debug){
  int return_code;
  unsigned int data_read;

  // FIXME : This is a strange hack to get to read out - simple c++ that needs fixing
  return_code = crate->vme_A32D32_read (addr, &data_read);
  data = data_read;

  if(debug){
    printf("ReadSlaveReg :  \t\taddr = 0x%08X    \tdata = 0x%08X    \treturn_code = 0x%08X \n", addr, data, return_code);
  }

  // TODO : add exception for a bad return value
}

void WriteSlaveReg( sis3153eth* crate, unsigned int addr, unsigned int data, bool debug){
  int return_code;
  return_code = crate->vme_A32D32_write (addr, data);
  if(debug){
    printf("WriteSlaveReg :  \t\taddr = 0x%08X    \tdata = 0x%08X    \treturn_code = 0x%08X \n", addr, data, return_code);
  }

  // TODO : add exception for a bad return value
}

int sis3153_TestComm( sis3153eth* crate, bool debug ){
  std::cout<<"\n\n[] sis3153_TestComm"<<std::endl;

  int return_code;
  UINT data;

  // read the basic configuration information of the interface board
  ReadMasterReg(crate, SIS3153_CONTROL_STATUS, data, debug);
  ReadMasterReg(crate, SIS3153_MODID_VERSION, data, debug);
  ReadMasterReg(crate, SIS3153_SERIAL_NUMBER_REG, data, debug);
  ReadMasterReg(crate, SIS3153_LEMO_IO_CTRL_REG, data, debug);
  ReadMasterReg(crate, SIS3153_VME_MASTER_CONTROL_STATUS, data, debug);
  ReadMasterReg(crate, SIS3153_VME_MASTER_CYCLE_STATUS, data, debug);
  ReadMasterReg(crate, SIS3153_VME_INTERRUPT_STATUS, data, debug);

  // turn the LED A on and off to demonstrate functionality of communication
  std::cout<<"\n\n====================================="<<std::endl;
  std::cout<<"Testing interface card communications with LED A on and off"<<std::endl;
  std::cout<<"====================================="<<std::endl;
  std::cout<<"Turn LED-A on"<<std::endl;
  WriteMasterReg(crate, SIS3153_CONTROL_STATUS, 0x1, debug);
  usleep(100000);
  std::cout<<"Turn LED-A off"<<std::endl;
  WriteMasterReg(crate, SIS3153_CONTROL_STATUS, 0x10000, debug);
  usleep(100000);
  std::cout<<"Turn LED-A on"<<std::endl;
  WriteMasterReg(crate, SIS3153_CONTROL_STATUS, 0x1, debug);
  std::cout<<"The light should still be on for LED A on the interface card"<<std::endl;

  return 0;
}


