#include "Helper_Event.h"

int Payload_GetEventSize( uint32_t raw_payload[], bool debug ){
  std::cout<<"\n\n[] Payload_GetEventSize"<<std::endl;

  // TODO : This assumes it was a successful read with a good header

  unsigned int eSize = raw_payload[0] & 0x0FFFFFF;

  std::cout<<"EventSize : "<<std::dec<<eSize<<std::endl;

  return eSize;
}

int Payload_GetChannelMask( uint32_t raw_payload[], std::vector< bool >& channel_mask, bool debug){
  std::cout<<"\n\n[] Payload_GetChannelMask"<<std::endl;

  channel_mask.clear();

  // assume a channel is not enabled
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    channel_mask.push_back(false);
  }

  // save the channel masks
  // TODO : Make the number of channels not hardcoded
  for(int iChan=0; iChan<8; iChan++){
    channel_mask.at(iChan) = (raw_payload[1] & (1<<iChan)) >> iChan;
  }

  for(int iChan=8; iChan<16; iChan++){
    channel_mask.at(iChan) = (raw_payload[2] & (1<<(16+iChan))) >> (16+iChan);
  }

  // print out the channel mask in debug mode
  if(debug){
    for(int iChan=0; iChan<NCHANNELS; iChan++){
      std::cout<<"ChannelMask : "<<channel_mask.at(iChan)<<std::endl;
    }
  }

  return 0;
}

int Payload_GetBufferLength( uint32_t raw_payload[], int& words_per_channel, int& samples_per_channel, bool debug){
  std::cout<<"\n\n[] Payload_GetBufferLength"<<std::endl;

  // get channel mask
  std::vector<bool> temp_channel_mask;
  Payload_GetChannelMask(raw_payload, temp_channel_mask, true);

  // find number of summed channels
  int n_channel_active=0;
  for(int iChan=0; iChan<temp_channel_mask.size(); iChan++){
    if(temp_channel_mask.at(iChan)==true)
      n_channel_active++;
  }

  // get event size
  int eSize=Payload_GetEventSize( raw_payload );

  // subtract 4 for the header
  int eSizeMod = eSize-4;

  // divide modified event size by number of channels
  words_per_channel = eSizeMod/n_channel_active;

  samples_per_channel = 2*words_per_channel;

  return 0;
}

int Payload_Dump( uint32_t raw_payload[], bool debug ){
  std::cout<<"\n\n[] Payload_Dump"<<std::endl;

  unsigned int EventSize = Payload_GetEventSize( raw_payload, debug );

  std::cout<<"DUMP - Header"<<std::endl;
  for(int iWord=0; iWord<4; iWord++){
    std::cout<<"head "<<std::dec<<iWord<<" : "<<std::setfill('0')<<std::setw(8)<<std::hex<<raw_payload[iWord]<<std::endl;
  }

  std::cout<<"DUMP - Data"<<std::endl;
  for(int iWord=4; iWord<EventSize; iWord++){
    std::cout<<"data "<<std::dec<<iWord<<" : "<<std::setfill('0')<<std::setw(8)<<std::hex<<raw_payload[iWord]<<std::endl;
  }

  return 0;
}

int Payload_Parse( uint32_t raw_payload[], std::vector< std::vector< unsigned int >>& data_parsed, bool debug){
  std::cout<<"\n\n[] Payload_Parse"<<std::endl;

  // clear the data that was in the output
  data_parsed.clear();

  int eSize = Payload_GetEventSize(raw_payload);

  std::vector<bool> channel_mask;
  Payload_GetChannelMask(raw_payload, channel_mask, true);

  int words_per_channel;
  int samples_per_channel;
  Payload_GetBufferLength(raw_payload, words_per_channel, samples_per_channel, true);

  std::cout<<"Samples : "<<samples_per_channel<<"  "<<words_per_channel<<std::endl;

  // TODO - put some checks that the number of channels and the buffer length makes sense

  // now parse the event data buffer depending on the channel mask info
  unsigned int currentLoc=4;

  for(int iChan=0; iChan<NCHANNELS; iChan++){

    std::cout<<"Location : "<<iChan<<" "<<currentLoc<<std::endl;

    data_parsed.push_back({});

    if(channel_mask.at(iChan)==1){
      std::cout<<"Channel good to read : "<<iChan<<" start = "<<currentLoc<<std::endl;
      for(int iDat=currentLoc; iDat<currentLoc+words_per_channel; iDat++){

        // two readings are stored in one word
        unsigned int chData = raw_payload[iDat];

        unsigned int ev0    = (chData & 0xFFFF0000) >> 16;  // first half of event doublet
        unsigned int ev1    = (chData & 0x0000FFFF);        // second half of event doublet

        data_parsed.at(iChan).push_back( ev1 );
        data_parsed.at(iChan).push_back( ev0 );

      }

      // move where we are looking
      currentLoc += words_per_channel;
    }

  }



  return 0;
}



int Data_Dump( unique_ptr<EventFragment>& data, int n_dump, bool debug){
  std::cout<<"\n\n[] Data_Dump"<<std::endl;

  std::cout<<"\nHeader(FASER)"<<std::endl;

  std::cout<<" marker         - "<<std::hex<<unsigned(data->header.marker)         <<std::endl;
  std::cout<<" fragment_tag   - "<<std::hex<<unsigned(data->header.fragment_tag)   <<std::endl;
  std::cout<<" trigger_bits   - "<<std::hex<<data->header.trigger_bits   <<std::endl;
  std::cout<<" version_number - "<<std::hex<<data->header.version_number <<std::endl;
  std::cout<<" header_size    - "<<std::hex<<data->header.header_size    <<std::endl;
  std::cout<<" payload_size   - "<<std::hex<<data->header.payload_size   <<std::endl;
  std::cout<<" payload_size   - "<<std::hex<<data->header.payload_size   <<std::endl;
  std::cout<<" source_id      - "<<std::hex<<data->header.source_id      <<std::endl;
  std::cout<<" event_id       - "<<std::hex<<data->header.event_id       <<std::endl;
  std::cout<<" bc_id          - "<<std::hex<<data->header.bc_id          <<std::endl;
  std::cout<<" status         - "<<std::hex<<data->header.status         <<std::endl;
  std::cout<<" timestamp      - "<<std::hex<<data->header.timestamp      <<std::endl;

  std::cout<<"\nData Payload - Header"<<std::endl;
  for(int iPay=0; iPay<4; iPay++){
    std::cout<<" "<<std::dec<<iPay<<" : "<<std::hex<<unsigned(data->payload[iPay])<<std::endl;
  }

  std::cout<<"\nData Payload - Data ["<<n_dump<<" words] Beginning"<<std::endl;
  for(int iPay=4; iPay<4+n_dump; iPay++){
    std::cout<<" "<<std::dec<<iPay<<" : "<<std::hex<<unsigned(data->payload[iPay])<<std::endl;
  }

  std::cout<<"\nData Payload - Data ["<<n_dump<<" words] End"<<std::endl;
  for(int iPay=data->header.payload_size-n_dump; iPay<data->header.payload_size; iPay++){
    std::cout<<" "<<std::dec<<iPay<<" : "<<std::hex<<unsigned(data->payload[iPay])<<std::endl;
  }

  return 0;
}


int Data_Write( unique_ptr<EventFragment>& data, std::string outputpath, bool debug){
  std::cout<<"\n\n[] Data_Write"<<std::endl;

  std::fstream fout;
  fout.open(outputpath.c_str(), ios::out);

  std::cout<<"\nHeader(FASER)"<<std::endl;

  fout<<"faser-marker         - "<<std::hex<<unsigned(data->header.marker)         <<std::endl;
  fout<<"faser-fragment_tag   - "<<std::hex<<unsigned(data->header.fragment_tag)   <<std::endl;
  fout<<"faser-trigger_bits   - "<<std::hex<<data->header.trigger_bits             <<std::endl;
  fout<<"faser-version_number - "<<std::hex<<data->header.version_number           <<std::endl;
  fout<<"faser-header_size    - "<<std::hex<<data->header.header_size              <<std::endl;
  fout<<"faser-payload_size   - "<<std::hex<<data->header.payload_size             <<std::endl;
  fout<<"faser-payload_size   - "<<std::hex<<data->header.payload_size             <<std::endl;
  fout<<"faser-source_id      - "<<std::hex<<data->header.source_id                <<std::endl;
  fout<<"faser-event_id       - "<<std::hex<<data->header.event_id                 <<std::endl;
  fout<<"faser-bc_id          - "<<std::hex<<data->header.bc_id                    <<std::endl;
  fout<<"faser-status         - "<<std::hex<<data->header.status                   <<std::endl;
  fout<<"faser-timestamp      - "<<std::hex<<data->header.timestamp                <<std::endl;

  std::cout<<"\nData Payload - Header"<<std::endl;
  fout<<"payload-header0 : "<<std::hex<<unsigned(data->payload[0])<<std::endl;
  fout<<"payload-header1 : "<<std::hex<<unsigned(data->payload[1])<<std::endl;
  fout<<"payload-header2 : "<<std::hex<<unsigned(data->payload[2])<<std::endl;
  fout<<"payload-header3 : "<<std::hex<<unsigned(data->payload[3])<<std::endl;


  std::vector< std::vector< unsigned int >> parsed_data;
  Payload_Parse(data->payload, parsed_data );

  std::vector<bool> channel_mask;
  Payload_GetChannelMask(data->payload, channel_mask, true);

  int words_per_channel;
  int samples_per_channel;
  Payload_GetBufferLength(data->payload, words_per_channel, samples_per_channel, true);



  std::cout<<"\nData Payload"<<std::endl;

  // reading value
  fout<<"payload-data "<<setw(10)<<"reading";
  // write data for each channel that is active and -1 if its not active
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    std::string line = "chan-"+to_string(iChan);
    fout<<setw(13)<<line;
  }
  fout<<"\n";

  for(int iData=0; iData<samples_per_channel; iData++){

    std::cout<<"Writing out : "<<iData<<std::endl;

    // reading value
    fout<<"PAYLOAD-DATA "<<setw(10)<<std::dec<<iData<<"   ";

    // write data for each channel that is active and -1 if its not active
    for(int iChan=0; iChan<NCHANNELS; iChan++){
      if(channel_mask.at(iChan)==1){
        fout<<setw(10)<<parsed_data.at(iChan).at(iData)<<"   ";
      }
      else{
        fout<<setw(10)<<"-1"<<"   ";
      }
    }

    // write a newline
    fout<<"\n";

  }

  fout.close();

  return 0;
}
