//============================================================================
// Name        : DigitizerHelper
// Author      : Sam Meehan
// Version     : v0.0
// Copyright   : Your copyright notice
// Description : Helper functions for the FASER digitizer readout
//============================================================================

#ifndef  COMM_VX1730_INCLUDE_H
#define  COMM_VX1730_INCLUDE_H

#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <ctime>

#include "Registers_vx1730.h"

#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"

using namespace std;

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include <chrono>
using namespace std::chrono;

class vx1730 {

  public:

    vx1730(char ip[], unsigned int vme_base);
    ~vx1730();

    // basic components needed to communicate with board registers
    sis3153eth *crate; // this is the crate instance
    unsigned int base_address; // this is the base address of the digitizer - configured physically on the board

    // monitoring
    int TestComm();
    int DumpConfig();
    int MonitorTemperature( std::string outputfile, bool debug=false);
    
    // control
    int Configure(json config, bool debug=false);
    int Reset(bool debug=false);
    int StartAcquisition( bool debug=false );
    int StopAcquisition( bool debug=false );
  
    // trigger and event readout
    int SendSWTrigger( bool debug=false );
    int DumpEventCount( bool debug=false );
    int ReadRawEvent( uint32_t raw_payload[], bool debug=false );
    int DumpFrontEvent( std::string outputfile, bool debug=false);

    // helper functions
    int GetBufferLength( unsigned int code );
};

#endif

