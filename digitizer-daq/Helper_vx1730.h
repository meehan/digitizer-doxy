//============================================================================
// Name        : DigitizerHelper
// Author      : Sam Meehan
// Version     : v0.0
// Copyright   : Your copyright notice
// Description : Helper functions for the FASER digitizer readout
//============================================================================

#ifndef  HELPER_VX1730_INCLUDE_H
#define  HELPER_VX1730_INCLUDE_H

#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <ctime>

#include "Registers_vx1730.h"

#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"

using namespace std;

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include <chrono>
using namespace std::chrono;

// these should become the library eventually
int vx1730_TestComm( sis3153eth* crate, UINT base_address, bool debug=false);
int vx1730_Configure( sis3153eth* crate, UINT base_address, json config, bool debug=false);
int vx1730_Reset( sis3153eth* crate, UINT base_address, bool debug=false);
int vx1730_StartAcquisition( sis3153eth* crate, UINT base_address, bool debug=false );
int vx1730_StopAcquisition( sis3153eth* crate, UINT base_address, bool debug=false );
int vx1730_DumpEventCount( sis3153eth* crate, UINT base_address, bool debug=false );
int vx1730_SendSWTrigger( sis3153eth* crate, UINT base_address, bool debug=false );
int vx1730_ReadRawEvent( sis3153eth* crate, UINT base_address, uint32_t raw_payload[], bool debug=false );
int vx1730_MonitorTemperature( sis3153eth* crate, UINT base_address, std::string outputfile, bool debug=false);
int vx1730_GetBufferLength( unsigned int code );
int vx1730_DumpConfig( sis3153eth* crate, UINT base_address );
int vx1730_DumpFrontEvent( sis3153eth* crate, UINT base_address, std::string outputfile, bool debug=false);

#endif

