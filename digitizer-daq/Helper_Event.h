#ifndef  HELPER_EVENT_INCLUDE_H
#define  HELPER_EVENT_INCLUDE_H

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <cstring>
#include <memory>
#include <fstream>

using namespace std;

#define MAXFRAGSIZE 16000
#define NCHANNELS 16

// emulation of the FASER-DAQ event fragment structures
// remove this later
enum EventMarkers {
  FragmentMarker = 0xAA,
  EventMarker = 0xBB
};

enum EventTags {
  PhysicsTag = 0x00,
  CalibrationTag = 0x01,
  MonitoringTag = 0x02,
  TLBMonitoringTag = 0x03
};

const uint16_t EventFragmentVersion = 0x0001;
const uint16_t EventHeaderVersion = 0x0001;


struct EventFragmentHeader {
  uint8_t  marker;
  uint8_t  fragment_tag;
  uint16_t trigger_bits;
  uint16_t version_number;
  uint16_t header_size;
  uint32_t payload_size;
  uint32_t source_id;
  uint64_t event_id;    //could be reduced to 32 bits
  uint16_t bc_id;
  uint16_t status;
  uint64_t timestamp;
}  __attribute__((__packed__));

struct EventFragment {
  EventFragmentHeader header;
  uint32_t payload[];    // FIXME : did we want this to be an uint8_t or a uint32_t ????? @brian
} __attribute__((__packed__));


int Payload_GetEventSize( uint32_t raw_payload[], bool debug=false );
int Payload_GetChannelMask( uint32_t raw_payload[], std::vector< bool >& channel_mask, bool debug=false );
int Payload_GetBufferLength( uint32_t raw_payload[], int& words_per_channel, int& samples_per_channel, bool debug);
int Payload_Dump( uint32_t raw_payload[], bool debug=false );
int Payload_Parse( uint32_t raw_payload[], std::vector< std::vector< unsigned int >>& data_parsed, bool debug=false);

int Data_Dump( unique_ptr<EventFragment>& data, int n_dump, bool debug=false );
int Data_Write( unique_ptr<EventFragment>& data, std::string outputpath, bool debug=false );

#endif



