//============================================================================
// Name        : DigitizerHelper
// Author      : Sam Meehan
// Version     : v0.0
// Copyright   : Your copyright notice
// Description : Helper functions for the FASER digitizer readout.
//============================================================================


#include "Helper.h"




// for the generic input parsing
void genHelp(){

  std::cout<<"\n\nThis is the help screen for your vx1730 digitizer command line running"<<std::endl;
  std::cout<<"You must provide the following arguments : "<<std::endl;
  std::cout<<" -c [CONFIGFILE] : CONFIGFILE is the path to the json formatted config file"<<std::endl;
  std::cout<<" -o [OUTPUTPATH] : OUTPUTPATH is the location of the directory that you wish"<<std::endl;
  std::cout<<"                   to use to store your output files\n\n"<<std::endl;

}

json openJsonFile(std::string filepath){
  std::ifstream file(filepath);
  if (!file) {
    throw std::runtime_error("Could not open json config file : "+filepath);
  }
  json j;
  try {
    j = json::parse(file);
  }catch (json::parse_error &e) {
    throw std::runtime_error(e.what());
  }
  file.close();
  return j;
}

std::string ConvertIntToWord(unsigned int word, bool debug){

  std::string wordout;

  for(int iBit=31; iBit>=0; iBit--){

    int bitval = (word & (1<<iBit)) >> iBit;

    if(debug)
      std::cout<<"bitval : "<<std::dec<<iBit<<"  "<<bitval<<std::endl;

    wordout += to_string(bitval);

    if(iBit%4==0)
      wordout += " ";

  }

  if(debug)
    std::cout<<"wordout : "<<wordout<<std::endl;

  return wordout;
}

void SetBitToZero(unsigned int& word, int bitval){
  word &= ~( 0 << bitval );
}

void SetBitToOne(unsigned int& word, int bitval){
  word |=  ( 1 << bitval );
}

void SetBit(unsigned int& word, int bit_location, int bit_val){

    if(bit_val==0){
        word &= ~( 0 << bit_location );
    }
    else if(bit_val==1){
        word |=  ( 1 << bit_location );
    }
    else{
        std::cout<<"This is not a valid bit value to set - exitting"<<std::endl;
        exit(18);
    }

}

std::string GetDateVerboseString(){

  std::string verboseDate;
  
  using namespace std;
  using namespace std::chrono;
  typedef duration<int, ratio_multiply<hours::period, ratio<24> >::type> days;
  system_clock::time_point now = system_clock::now();
  system_clock::duration tp = now.time_since_epoch();
  days d = duration_cast<days>(tp);
  tp -= d;
  hours h = duration_cast<hours>(tp);
  tp -= h;
  minutes m = duration_cast<minutes>(tp);
  tp -= m;
  seconds s = duration_cast<seconds>(tp);
  tp -= s;
  std::cout << d.count() << "d " << h.count() << ':'
            << m.count() << ':' << s.count();
  std::cout << " " << tp.count() << "["
            << system_clock::duration::period::num << '/'
            << system_clock::duration::period::den << "]\n";

  time_t tt = system_clock::to_time_t(now);
  tm utc_tm = *gmtime(&tt);
  tm local_tm = *localtime(&tt);
  std::cout << utc_tm.tm_year+1900 << std::endl;
  std::cout << utc_tm.tm_mon+1 << std::endl;
  std::cout << utc_tm.tm_mday << std::endl;
  std::cout << utc_tm.tm_hour << std::endl;
  std::cout << utc_tm.tm_min << std::endl;
  std::cout << utc_tm.tm_sec << std::endl;
  
  //desciptor
  verboseDate += "Date";
  
  // year
  verboseDate += std::to_string( utc_tm.tm_year+1900 );
  
  // month
  if(utc_tm.tm_mon+1 < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_mon+1 );
  }
  else
    verboseDate += std::to_string( utc_tm.tm_mon+1 );
    
  // day
  if(utc_tm.tm_mon+1 < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_mday );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_mday );
  }
  
  //desciptor
  verboseDate += "_Time";
  
  // hour
  if(utc_tm.tm_hour < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_hour );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_hour );
  }

  // minute
  if(utc_tm.tm_min < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_min );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_min );
  }

  // second
  if(utc_tm.tm_sec < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_sec );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_sec );
  }


  return verboseDate;
}

void Wait(float nseconds){
    std::cout<<"Waiting for : "<<nseconds<<" seconds"<<std::endl;
    int nmicroseconds = floor(nseconds*1000000.0);
    usleep(nmicroseconds);
}